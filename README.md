## Jak zacząć 
Witaj! W celu uruchomienia aplikacji do obsługi bazy danych sprzętu medycznego wykorzystywanego w szpitalu, należy pobrać oraz rozpakować archiwum o nazwie EquimedApp.rar, znajdujące się w commicie o nazwie _Finally version of portable application_ (numer e7ba22d). Aby aplikacja była w pełni kompatybilna, jeśli chodzi o interfejs użytkownika, polecamy skopiować czcionki dostępne w folderze _/fonts_ oraz wkleić je do folderu _Czcionki_ (Panel Sterowania --> Wygląd i personalizacja --> Czcionki).
Następnie należy uruchomić plik o nazwie **equimed.exe**. Jest to wersja portable aplikacji, więc otworzy się okno aplikacji. 
### Logowanie ###
Login     | Hasło
--------- | -------------
Admin     | Admin
L123      | L123
S8579     | S8579
I7589     | I7589

Każda z osób na którą się można zalogować ma inne funkcje – zachęcamy do przetestowania ich wszystkich!

### Wzorce projektowe ###
W projekcie wykorzystane są trzy wzorce projektowe.
Pierwszym z nich jest _Singleton_. Wywodzi się on z C++, gdzie był zastępcą zmiennej globalnej. Wykorzystaliśmy jego cechę zapewnienia globalnego dostępu do stworzonego obiektu. W momencie logowania się użytkownika zapisujemy wszystkie dane użytkownika - na tej podstawie wyświetlane są wszystkie funkcje dostępne dla niego. W momencie wylogowania się użytkownika zmienna jest zerowana - aż do momentu następnego logowania.
Drugim wzorcem projektowym jest _AbstractFactory_. Jest do wzorzec do dostarczenia interfejsu dla tworzonych różnych obiektów tego samego typu. Umożliwia tworzenie różnych, powiązanych ze sobą podobiektów przez jeden obiekt; kładzie nacisk na całe rodziny podobiektów. Wykorzystaliśmy ten wzorzec w momencie tworzenia i wyświetlania menu dla różnych poziomów dostępu.
Trzecim jest wzorzec _Proxy_. Jest do wzorzec wykorzystywany do utworzenia obiektu zastępującego inny obiekt. Stosowany jest on w celu kontrolowanego tworzenia na żądanie kosztownych obiektów oraz kontroli dostępu do nich. Może on spełniać 4 funkcje: pełnomocnika zdalnego (do wykonywania zdalnych obiektów), pełnomocnika wirtualnego (gdy nie chcemy tworzyć obiektu klasy do momentu wywołania jednej z metod), pełnomocnika ochraniającego (w celu kontroli dostępu do pewnego obiektu), pełnomocnika sprytnego (gdy poza akcją wykonywaną przez obiekt chcemy wykonać dodatkowe akcje związane z jego metodami). W naszym projekcie została użyta funkcja pełnomocnika ochraniającego - wykorzystana w połączeniu z bazą danych jako ochrona i odizolowanie od obiektu DbServices.