#include "user.h"

QString User::getLogin() const
{
    return login;
}

void User::setLogin(const QString &value)
{
    login = value;
}

QString User::getPassword() const
{
    return password;
}

void User::setPassword(const QString &value)
{
    password = value;
}

QString User::getName() const
{
    return name;
}

void User::setName(const QString &value)
{
    name = value;
}

QString User::getSurname() const
{
    return surname;
}

void User::setSurname(const QString &value)
{
    surname = value;
}

QString User::getPhone() const
{
    return phone;
}

void User::setPhone(const QString &value)
{
    phone = value;
}

QString User::getEmail() const
{
    return email;
}

void User::setEmail(const QString &value)
{
    email = value;
}

QString User::getPesel() const
{
    return pesel;
}

void User::setPesel(const QString &value)
{
    pesel = value;
}

QString User::getRole() const
{
    return role;
}

void User::setRole(const QString &value)
{
    role = value;
}

QString User::getId_branch() const
{
    return id_branch;
}

void User::setId_branch(const QString &value)
{
    id_branch = value;
}

User::User(QString login, QString password, QString name, QString surname,
           QString phone, QString email, QString pesel, QString role, QString id_branch)
{
    this->login = login;
    this->password = password;
    this->name = name;
    this->surname = surname;
    this->phone = phone;
    this->email = email;
    this->pesel = pesel;
    this->role = role;
    this->id_branch = id_branch;

}

