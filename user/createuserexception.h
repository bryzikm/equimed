#ifndef CREATEUSEREXCEPTION_H
#define CREATEUSEREXCEPTION_H
#include <QException>

class CreateUserException : public QException
{
protected:
    QString message;
public:
    CreateUserException();
    QString getMessage(){ return message; }
    void raise() const{throw *this; }
    CreateUserException *clone() const { return new CreateUserException(*this); }
};

class EmptyFieldUserException : public CreateUserException
{
public:
    EmptyFieldUserException(QString message_){ message = message_;}
    EmptyFieldUserException *clone() const { return new EmptyFieldUserException(*this); }
};


#endif // CREATEUSEREXCEPTION_H
