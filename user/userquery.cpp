#include "userquery.h"
#include <QDebug>
#include <QSqlError>

UserQuery::UserQuery()
{
}

bool UserQuery::create_user(User user)
{
    QString sql_request = "insert into user (login, password, name, surname, "
                          " phone, email, id_branch, pesel, role ) "
                          " values('"+user.getLogin()+"', '"+user.getPassword()+"',"
                          " '"+user.getName()+"', '"+user.getSurname()+"', '"+user.getPhone()+"', "
                          " '"+user.getEmail()+"', '"+user.getId_branch()+"', '"+user.getPesel()+"' ,"
                          " '"+user.getRole()+"') ";

    sql_query.prepare(sql_request);
    return sql_query.exec();
}

bool UserQuery::find_user_by_login(QString login)
{
    QString sql_request = "select * from user where login='"+login+"'";
    sql_query.prepare(sql_request);
    sql_query.exec();
    if(sql_query.next()!= NULL)
        return true;
    else
        return false;
}

bool UserQuery::delete_user_by_login(QString login)
{
     QString sql_request = "delete from user where login='"+login+"'";
     sql_query.prepare(sql_request);
     return sql_query.exec();
}

bool UserQuery::edit_user(QString login, QString name, QString surname, QString phone,
                          QString email, QString pesel, QString id_branch)
{
    QString sql_request = "update user set "
                          "login = '"+login+"',"
                          "name = '"+name+"',"
                          "surname = '"+surname+"', "
                          "phone = '"+phone+"',"
                          "email = '"+email+"',"
                          "pesel = '"+pesel+"',"
                          "id_branch = '"+id_branch+"'"
                          " where rowid = (select rowid from user where "
                          "login = '"+login+"')";
    qDebug()<<sql_request;
    sql_query.prepare(sql_request);
    return sql_query.exec();
}
