#ifndef USER_QUERY_H
#define USER_QUERY_H
#include "user.h"

#include <QSqlQuery>


class UserQuery
{
public:
    UserQuery();
    //fields:
private:
    QSqlQuery sql_query;

    //method:
public:
    bool create_user(User user);
    bool find_user_by_login(QString login);
    bool delete_user_by_login(QString login);
    bool edit_user(QString login, QString name, QString surname, QString phone, QString email, QString id_branch, QString pesel);
};

#endif // USER_QUERY_H
