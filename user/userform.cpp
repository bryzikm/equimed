#include "userform.h"
#include "ui_userform.h"
#include "user.h"
#include "usermanager.h"
#include "dbservices.h"
#include <QDebug>
#include <QMessageBox>
#include <hospitalWard/hospitalwardservice.h>
#include <memory>
#include "createuserexception.h"
void UserForm::add_role_to_select_box()
{
    foreach (QString item, role) {
        ui->accessEdit->addItem(item);
    }
}

void UserForm::add_branch_id_to_select_box()
{
    std::unique_ptr<HospitalWardService> hospitalWardService(new HospitalWardService);
    QMap<int, QString> hospitalWards = hospitalWardService->getAllWards();

    for(auto row : hospitalWards.toStdMap()) {
        this->ui->id_odzialuEdit->addItem(row.second, QVariant(row.first));
    }
}

UserForm::UserForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UserForm)
{
    ui->setupUi(this);
    ui->peselEdit->setValidator( new QIntValidator(0, 99999999999, this) );
    ui->phoneEdit->setValidator( new QIntValidator(0, 999999999, this) );
    add_role_to_select_box();
    add_branch_id_to_select_box();
}

UserForm::~UserForm()
{
    delete ui;
}

void UserForm::is_valid()
{
    if (ui->loginEdit->text().isEmpty())
        throw EmptyFieldUserException(QString("Login nie moze być pusty"));
    else if (ui->passwordEdit->text().isEmpty())
        throw EmptyFieldUserException(QString("Hasło nie moze być puste"));
    else if (ui->nameEdit->text().isEmpty())
        throw EmptyFieldUserException(QString("Imię nie moze być puste"));
    else if (ui->surnameEdit->text().isEmpty())
        throw EmptyFieldUserException(QString("Nazwisko nie moze być puste"));
    else if (ui->phoneEdit->text().isEmpty())
        throw EmptyFieldUserException(QString("Telefon nie moze być pusty"));
    else if (ui->emailEdit->text().isEmpty())
        throw EmptyFieldUserException(QString("Email nie moze być pusty"));
    else if (ui->peselEdit->text().isEmpty())
        throw EmptyFieldUserException(QString("Pesel nie moze być pusty"));
}

User UserForm::create_user_from_form()
{
    int index = this->ui->id_odzialuEdit->currentIndex();
    QString oddzialId = this->ui->id_odzialuEdit->itemData(index).toString();
    std::unique_ptr<User> user(new User(ui->loginEdit->text(),
                                        ui->passwordEdit->text(),
                                        ui->nameEdit->text(),
                                        ui->surnameEdit->text(),
                                        ui->phoneEdit->text(),
                                        ui->emailEdit->text(),
                                        ui->peselEdit->text(),
                                        ui->accessEdit->currentText(),
                                        oddzialId));

    return *user;
}

void UserForm::create_user(User user, UserManager *user_manager)
{
    if( user_manager->create_new_user(user))
        QMessageBox::information(this, "Zapisywanie", "Użytkownik został utworzony");
    else
        QMessageBox::critical(this, "Zapisywanie", "Użytkownik nie został utworzony!");
}

void UserForm::on_buttonBox_accepted()
{
    try
    {
        is_valid();
        User user = create_user_from_form();
        UserManager *user_manager = new UserManager();
        if(user_manager->find_user_by_login(user.getLogin()))
            QMessageBox::critical(this, "Zapisywanie", "Użytkownik z loginem: '"+user.getLogin()+"' już istnieje w bazie!");
        else
            create_user(user, user_manager);

        delete user_manager;

    }catch(CreateUserException &e)
    {
            QMessageBox::critical(this, "Błąd!", e.getMessage());
    }

}
