#include "userconstants.h"

#include <qstring.h>

UserConstants::UserConstants()
{

}
const QString UserConstants::ROLE_ADMIN = "admin";
const QString UserConstants::ROLE_SERVICEMAN = "serviceman";
const QString UserConstants::ROLE_STOCK_TAKER = "stock taker";
const QString UserConstants::ROLE_DOCTOR_NURSE = "doctor/nurse";

