#include "user.h"
#include "user/usermanager.h"
#include "user/userquery.h"

UserQuery *user_query;

UserManager::UserManager()
{
    user_query = new UserQuery();
}

bool UserManager::create_new_user(User user)
{
    return user_query->create_user(user);
}

bool UserManager::find_user_by_login(QString login)
{
    return user_query->find_user_by_login(login);
}

bool UserManager::delete_user_by_login(QString login)
{
    return user_query->delete_user_by_login(login);
}

bool UserManager::edit_user(QString login, QString name, QString surname, QString phone, QString email, QString id_branch, QString pesel)
{
    return user_query->edit_user(login, name, surname, phone, email, id_branch, pesel);
}
