#ifndef USER_H
#define USER_H
#include <QString>




class User
{
    // fields
private:
    QString login;
    QString password;
    QString name;
    QString surname;
    QString phone;
    QString email;
    QString pesel;
    QString role;
    QString id_branch;

    // method
public:
    User(QString login, QString password, QString name, QString surname,
         QString phone, QString email, QString pesel, QString role, QString id_branch);

    QString getLogin() const;
    void setLogin(const QString &value);
    QString getPassword() const;
    void setPassword(const QString &value);
    QString getName() const;
    void setName(const QString &value);
    QString getSurname() const;
    void setSurname(const QString &value);
    QString getPhone() const;
    void setPhone(const QString &value);
    QString getEmail() const;
    void setEmail(const QString &value);
    QString getPesel() const;
    void setPesel(const QString &value);
    QString getRole() const;
    void setRole(const QString &value);
    QString getId_branch() const;
    void setId_branch(const QString &value);
};
#endif // USER_H
