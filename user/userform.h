#ifndef USERPANEL_H
#define USERPANEL_H

#include "userconstants.h"
#include "user.h"
#include "usermanager.h"

#include <QAbstractButton>
#include <QDialog>
#include <QList>


namespace Ui {
class UserForm;
}

class UserForm : public QDialog
{
    Q_OBJECT

public:
    explicit UserForm(QWidget *parent = 0);
    ~UserForm();

    User create_user_from_form();
    void create_user(User user, UserManager *user_manager);

    void add_role_to_select_box();

    void add_branch_id_to_select_box();
    void is_valid();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::UserForm *ui;
    const QList<QString> role = {UserConstants::ROLE_ADMIN, UserConstants::ROLE_SERVICEMAN,
                                 UserConstants::ROLE_STOCK_TAKER, UserConstants::ROLE_DOCTOR_NURSE};

};

#endif // USERPANEL_H
