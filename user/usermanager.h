#ifndef USER_MANAGER_H
#define USER_MANAGER_H

#include "user.h"

#include <QString>



class UserManager
{
public:
    UserManager();
    bool create_new_user(User user);
    bool find_user_by_login(QString login);
    bool delete_user_by_login(QString login);
    bool edit_user(QString login, QString name, QString surname, QString phone, QString email, QString id_branch, QString pesel);
};

#endif // USER_MANAGER_H
