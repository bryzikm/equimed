#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <qstring.h>



class UserConstants
{
public:
    static const QString ROLE_ADMIN;
    static const QString ROLE_SERVICEMAN;
    static const QString ROLE_STOCK_TAKER;
    static const QString ROLE_DOCTOR_NURSE;

    UserConstants();
};

#endif // CONSTANTS_H
