#include "userdatawidget.h"
#include "ui_userdatawidget.h"
#include "csingleton.h"
#include "userservices.h"
#include <memory>

UserDataWidget::UserDataWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UserDataWidget)
{
    std::unique_ptr<UserServices> us(new UserServices);
    ui->setupUi(this);

    ui->label_8->setText(CSingleton::getInstance()->getLogin());
    ui->label_9->setText(CSingleton::getInstance()->getName());
    ui->label_10->setText(CSingleton::getInstance()->getSurname());
    ui->label_11->setText(QString::number(CSingleton::getInstance()->getPhone()));
    ui->label_12->setText(CSingleton::getInstance()->getEmail());
    ui->label_13->setText(CSingleton::getInstance()->getPesel());
    QString branch=us->findBranch(CSingleton::getInstance()->getIdBranch());
    ui->label_14->setText(branch);
}

UserDataWidget::~UserDataWidget()
{
    delete ui;
}
