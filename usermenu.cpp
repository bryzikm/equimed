#include "usermenu.h"
#include <QObject>
#include <QWidget>
#include <ui_userform.h>
#include "userdatawidget.h"
#include <mainwindow.h>
#include "equipment/form/newequipmentform.h"
#include "user/userconstants.h"
#include <user/userform.h>

#include "admin/userlist.h"
#include "equipment/equiplist.h"
#include "equipment/loanequiplist.h"
#include "equipment/stocktakerequiplist.h"
#include "equipment/searcher/equipmentsearcher.h"
#include "equipment/damageequiplist.h"

#include "menu/menuabstractfactory.h"
#include "menu/adminmenufactory.h"
#include "menu/doctormenufactory.h"
#include "menu/servicemanmenufactory.h"
#include "menu/stocktakermenufactory.h"

#include <memory>

UserMenu::UserMenu()
{}

void UserMenu:: createMenu(MainWindow *mainwindow, QString role)
{
    mw = mainwindow;

    MenuAbstractFactory *factory;
    if(role == UserConstants::ROLE_ADMIN){
        factory = new AdminMenuFactory(mw);
    }
    if(role == UserConstants::ROLE_DOCTOR_NURSE){
        factory = new DoctorMenuFactory(mw);
    }
    if(role == UserConstants::ROLE_STOCK_TAKER){
        factory = new StocktakerMenuFactory(mw);
    }
    if(role == UserConstants::ROLE_SERVICEMAN){
        factory = new ServicemanMenuFactory(mw);
    }

    QActionGroup* actionGroupLocal = factory->getMenu();
    QList<QAction*> actions = actionGroupLocal->actions();

    for(int i = 0; i < actions.size(); i++)
    {
        mw->menuBar()->addAction(actions[i]);
    }

    connect(actionGroupLocal, SIGNAL(triggered(QAction*)),this, SLOT(actionEvent(QAction*)));

    delete factory;
}

void UserMenu::mainPage()
{
    UserDataWidget *widget= new UserDataWidget;
    mw->addWidgets(widget);
}

void UserMenu::addEquipment() {
    std::unique_ptr<NewDeviceForm> newDeviceForm(new NewDeviceForm(this));
    qDebug()<<newDeviceForm->exec();

}

void UserMenu::logout(){
    mw->mainParentForm->show();
    mw->close();
}

void UserMenu::userList()
{
    UserList *widget= new UserList;
    mw->addWidgets(widget);
}

void UserMenu::equipmentList()
{
    if(CSingleton::getInstance()->getRole() == UserConstants::ROLE_STOCK_TAKER)
    {
        StockTakerEquipList *widget = new StockTakerEquipList;
        mw->addWidgets(widget);
    }
    else
    {
        EquipList *widget= new EquipList;
        mw->addWidgets(widget);
    }

}

void UserMenu::loanEquipList()
{
    LoanEquipList *widget= new LoanEquipList;
    mw->addWidgets(widget);
}

void UserMenu::damageEquipList() {
    DamageEquipList *widget = new DamageEquipList;
    mw->addWidgets(widget);
}

void UserMenu::searchEquipment()
{
    EquipmentSearcher *widget = new EquipmentSearcher;
    mw->addWidgets(widget);
}

void UserMenu::addUser()
{
    std::unique_ptr<UserForm> user_form(new UserForm(this));
    user_form->exec();
}

void UserMenu::actionEvent(QAction *act)
{
    QString name = act->objectName();
    qDebug()<<name;

    if(name == "mainPage" )
        UserMenu::mainPage();
    else if(name == "addEquipment"){
        UserMenu::addEquipment();
    } else if(name=="userList"){
        UserMenu::userList();
    } else if(name=="listOfEquipment"){
        UserMenu::equipmentList();
    } else if(name == "userList"){
         UserMenu::userList();
    } else if (name == "logout"){
        UserMenu::logout();
    } else if (name == "addUser"){
        UserMenu::addUser();
    } else if(name =="listOfLoanEquipment"){
        UserMenu::loanEquipList();
    } else if(name == "serchEquipment") {
        UserMenu::searchEquipment();
    } else if(name == "damageEquipmentList") {
        UserMenu::damageEquipList();
    } else {
        qDebug()<<"Error";
    }

}
