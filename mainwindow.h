#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "csingleton.h"
#include <QHBoxLayout>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    QMainWindow *mainParentForm;
    void addWidgets(QWidget *widget);
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void remove_existing_widgets();

private:
    Ui::MainWindow *ui;

};

#endif // MAINWINDOW_H
