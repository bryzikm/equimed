#ifndef DBPROXY_H
#define DBPROXY_H

#include <memory>

#include "connector.h"
#include "dbservices.h"


class DbProxy : public Connector
{
public:
    DbProxy();

    void connect_with_database();

private:
    DbServices *dbs = NULL;
};

#endif // DBPROXY_H
