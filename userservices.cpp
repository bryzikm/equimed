#include "userservices.h"
#include "csingleton.h"
#include <QList>

UserServices::UserServices()
{

}

QString UserServices::findBranch(int id_branch)
{
    QString branch;
    qry.prepare("select nazwa from oddzial where id_oddzialu='"+QString::number(id_branch)+"'");
    if(qry.exec()){
        while(qry.next())
            {
                branch=qry.value(0).toString();
            }
     }
    return branch;
}

QList<QString> UserServices::findAllUser(){

    QList<QString> users;
    QSqlQuery qryList;

    qryList.prepare("select * from user");
    if(qryList.exec()){
        while(qryList.next()){
            users.append(qryList.value("login").toString());
            users.append(qryList.value("name").toString());
            users.append(qryList.value("surname").toString());
            users.append(qryList.value("phone").toString());
            users.append(qryList.value("email").toString());
            users.append(qryList.value("pesel").toString());
            users.append(qryList.value("id_branch").toString());
        }
    }

    return users;
}
