#ifndef USERLIST_H
#define USERLIST_H

#include <QComboBox>
#include <QTableWidgetItem>
#include <QWidget>

#include <hospitalWard/hospitalwardservice.h>

namespace Ui {
class UserList;
}

class UserList : public QWidget
{
    Q_OBJECT

public:
    explicit UserList(QWidget *parent = 0);
    ~UserList();
    int get_row_quantity();

    void connect_delete_user_button();

    int delete_button_show_message(QTableWidgetItem* item);

    void confirm_delete_user_action(QTableWidgetItem* item);

    void get_user_data_from_database();    
    
    int set_table_column_width();
    
    void set_table_column_width(int row_quantity);

    void connect_edit_user_button();

    void confirm_edit_user_action(QTableWidgetItem *item);

    QList<QString> get_value_from_row(QTableWidgetItem* item);

    void set_delete_button(int j, QTableWidgetItem* deleteUser);

    void set_edit_button(int i, QTableWidgetItem* editUser);
    
    void set_login_table_item(int j, QTableWidgetItem* login);

    void set_branch_combo_box(int j, QComboBox* branchName);

    void get_branch_id_from_table(QList<QString> user_field_value, QTableWidgetItem* item);

    void set_name_table_item(int j, QTableWidgetItem* name);

    void set_pesel_table_item(int j, QTableWidgetItem* pesel);

public slots:
    void edit_button_clicked(QTableWidgetItem *item);
    void delete_button_clicked(QTableWidgetItem *item);

private:
    Ui::UserList *ui;
    QList<QString> users;
    HospitalWardService *hospitalWardService = new HospitalWardService();
    QList<QComboBox*> branches_combo_box;
    const int dataOfUser = 7;
    const int DELETE_BUTTON_COLUMN_NUMBER = 8;
    const int EDIT_BUTTON_COLUMN_NUMBER = 7;
    const int BUTTON_WIDTH = 100;
    const int BUTTON_HEIGHT = 40;
    const int QUANTITY_OF_VISIBLE_TABLE_VALUE = 6;
    const QString TOOL_TIP_ON_BLOCKED_FIELD = "To pole nie może zostać zmienione.";
};

#endif // USERLIST_H
