#include "userlist.h"
#include "ui_userlist.h"
#include "csingleton.h"
#include "userservices.h"

#include <QTableWidget>
#include <QTableWidgetItem>
#include <QString>
#include <QtSql>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QMessageBox>
#include <usermenu.h>
#include <memory>

#include <user/usermanager.h>

#include <hospitalWard/hospitalwardservice.h>

int UserList::delete_button_show_message(QTableWidgetItem* item)
{
    QMessageBox message_box;
    QString message = item->text() + " zostanie usunięty jesteś pewien?";
    message_box.setText(message);
    message_box.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
    return message_box.exec();
}

QList<QString> UserList::get_value_from_row(QTableWidgetItem* item)
{
    QList<QString> user_field_value;
    for(int i = 0;i<QUANTITY_OF_VISIBLE_TABLE_VALUE;i++)
        user_field_value.append(ui->tableWidget->model()->data(
            ui->tableWidget->model()->index(item->text().toInt(), i)).toString());

    int index = item->text().toInt();
    QVariant branch_variant = branches_combo_box[index]->itemData(branches_combo_box[index]->currentIndex());
    user_field_value.append(branch_variant.toString());
    return user_field_value;
}

void UserList::confirm_edit_user_action(QTableWidgetItem* item)
{
    QList<QString> user_field_value = get_value_from_row(item);
    std::unique_ptr<UserManager> user_manager(new UserManager);
    bool edited_user = user_manager->edit_user(user_field_value[0],
                                               user_field_value[1],
                                               user_field_value[2],
                                               user_field_value[3],
                                               user_field_value[4],
                                               user_field_value[5],
                                               user_field_value[6]);
    if(edited_user)
    {
        QMessageBox::information(this, "Edycja", "Dane użytkownika zostały zaktualizowane");
    }
    else
        QMessageBox::critical(this, "Edycja", "Błąd!");
}

void UserList::confirm_delete_user_action(QTableWidgetItem* item)
{
    if (delete_button_show_message(item) == QMessageBox::Yes)
    {
        UserManager *user_manager = new UserManager();
        bool delete_user = user_manager->delete_user_by_login(item->text());
        if(delete_user)
        {
            QMessageBox::information(this, "Usuń", "Użytkownik został usunięty");
            get_user_data_from_database();
        }
        else
            QMessageBox::critical(this, "Usuń", "Błąd!");
    }
}

void UserList::delete_button_clicked(QTableWidgetItem* item)
{
    for (int i=0; i<get_row_quantity(); i++)
    {
        if (ui->tableWidget->item(i, UserList::DELETE_BUTTON_COLUMN_NUMBER) == item)
        {
            qDebug()<<"delete";
            confirm_delete_user_action(item);
        }
    }

}
void UserList::edit_button_clicked(QTableWidgetItem* item)
{

    qDebug()<<get_row_quantity();
    qDebug()<<item->text()<<" ten";
    for (int i=0; i<get_row_quantity(); i++)
    {
        qDebug()<<ui->tableWidget->item(i, UserList::DELETE_BUTTON_COLUMN_NUMBER)->text();
        if (ui->tableWidget->item(i, UserList::EDIT_BUTTON_COLUMN_NUMBER) == item)
        {
            qDebug()<<item->text()<<"Wybrany";
            qDebug()<<"edit";
            confirm_edit_user_action(item);

        }
    }
}

int UserList::get_row_quantity()
{
    return (users.size()/dataOfUser);
}

void UserList::connect_delete_user_button()
{
    connect(ui->tableWidget, SIGNAL(itemClicked(QTableWidgetItem*)),
            this, SLOT(delete_button_clicked(QTableWidgetItem*)));
}

void UserList::set_table_column_width(int row_quantity)
{
    ui->tableWidget->setRowCount(row_quantity);
    ui->tableWidget->setColumnWidth(0, 70);
    ui->tableWidget->setColumnWidth(1, 98);
    ui->tableWidget->setColumnWidth(4, 140);
    ui->tableWidget->setColumnWidth(6, 175);
    ui->tableWidget->setColumnWidth(7, BUTTON_WIDTH);
    ui->tableWidget->setColumnWidth(8, BUTTON_WIDTH);
}


void UserList::connect_edit_user_button()
{
    connect(ui->tableWidget, SIGNAL(itemClicked(QTableWidgetItem*)),
            this, SLOT(edit_button_clicked(QTableWidgetItem*)));
}


void UserList::set_delete_button(int j, QTableWidgetItem* deleteUser)
{
    ui->tableWidget->setIconSize(QSize(BUTTON_WIDTH, BUTTON_HEIGHT));
    deleteUser->setSizeHint(QSize(BUTTON_WIDTH, BUTTON_HEIGHT));
    deleteUser->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    QPixmap pixmap(":/images/button-delete.png");
    QIcon qicon(pixmap);
    deleteUser->setIcon(qicon);
    deleteUser->setText(users.at(j));
}

void UserList::set_edit_button(int i, QTableWidgetItem* editUser)
{
    ui->tableWidget->setIconSize(QSize(BUTTON_WIDTH, BUTTON_HEIGHT));
    editUser->setSizeHint(QSize(BUTTON_WIDTH, BUTTON_HEIGHT));
    editUser->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    QPixmap pixmap(":/images/button-edit.png");
    QIcon qicon(pixmap);
    editUser->setIcon(qicon);
    editUser->setText(QString::number(i));
}

void UserList::set_login_table_item(int j, QTableWidgetItem* login)
{
    login->setText(users.at(j));
    login->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    login->setToolTip(TOOL_TIP_ON_BLOCKED_FIELD);
}

void UserList::set_branch_combo_box(int j, QComboBox* branchName)
{
    QMap<int, QString> branches = hospitalWardService->getAllWards();
    for(auto row : branches.toStdMap()) {
         branchName->addItem(row.second, QVariant(row.first));
    }
    int index = branchName->findData(users.at(j+6).toInt());
    branchName->setCurrentIndex(index);
    branches_combo_box.append(branchName);
}

void UserList::set_name_table_item(int j, QTableWidgetItem* name)
{
    name->setText(users.at(j+1));
    name->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    name->setToolTip(TOOL_TIP_ON_BLOCKED_FIELD);
}

void UserList::set_pesel_table_item(int j, QTableWidgetItem* pesel)
{
    pesel->setText(users.at(j+5));
    pesel->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    pesel->setToolTip(TOOL_TIP_ON_BLOCKED_FIELD);
}

void UserList::get_user_data_from_database()
{
    users = UserServices::findAllUser();
    int row_quantity = get_row_quantity();
    set_table_column_width(row_quantity);
    int j=0;
    while(j<users.size()){
        for(int i=0; i<row_quantity; i++){
            QTableWidgetItem* login = new QTableWidgetItem();
            QTableWidgetItem* name = new QTableWidgetItem();
            QTableWidgetItem* surname = new QTableWidgetItem();
            QTableWidgetItem* phone = new QTableWidgetItem();
            QTableWidgetItem* email = new QTableWidgetItem();
            QTableWidgetItem* pesel = new QTableWidgetItem();
            QComboBox* branchName = new QComboBox();
            QTableWidgetItem* editUser = new QTableWidgetItem();
            QTableWidgetItem* deleteUser = new QTableWidgetItem();


            set_login_table_item(j, login);
            set_name_table_item(j, name);
            surname->setText(users.at(j+2));
            phone->setText(users.at(j+3));
            email->setText(users.at(j+4));
            set_pesel_table_item(j, pesel);
            set_branch_combo_box(j, branchName);
            set_edit_button(i, editUser);
            set_delete_button(j ,deleteUser);

            ui->tableWidget->setItem(i,0,login);
            ui->tableWidget->setItem(i,1,name);
            ui->tableWidget->setItem(i,2,surname);
            ui->tableWidget->setItem(i,3,phone);
            ui->tableWidget->setItem(i,4,email);
            ui->tableWidget->setItem(i,5,pesel);
            ui->tableWidget->setCellWidget(i, 6, branchName);
            ui->tableWidget->setItem(i,7,editUser);
            ui->tableWidget->setItem(i,8,deleteUser);

            j=j+7;
        }
    }
    connect_delete_user_button();
    connect_edit_user_button();
}

UserList::UserList(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UserList)
{
    ui->setupUi(this);
    get_user_data_from_database();
}

UserList::~UserList()
{
    delete ui;
}
