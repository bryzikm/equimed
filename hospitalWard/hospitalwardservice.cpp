#include "hospitalwardservice.h"
#include <QSqlQuery>

HospitalWardService::HospitalWardService()
{
}

QMap<int, QString> HospitalWardService::getAllWards()
{
    QMap<int, QString> result;

    QSqlQuery qry;
    qry.prepare("SELECT id_oddzialu, nazwa FROM oddzial");
    qry.exec();
    while(qry.next())
    {
        result.insert(qry.value("id_oddzialu").toInt(), qry.value("nazwa").toString());
    }

    return result;
}


HospitalWardService::~HospitalWardService()
{

}


