#ifndef HOSPITALWARDSERVICE_H
#define HOSPITALWARDSERVICE_H
#include "dbmanager.h"

class HospitalWardService
{
public:
    HospitalWardService();
    QMap<int, QString> getAllWards();
    ~HospitalWardService();

private:
    DbManager *dbManager;

};

#endif // HOSPITALWARDSERVICE_H
