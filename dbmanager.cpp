#include "dbmanager.h"


DbManager::DbManager(){
    mydb = QSqlDatabase::addDatabase("QSQLITE");
    mydb.setDatabaseName("EquimedDB.db");
}

bool DbManager::open_connection()
{
    if(!mydb.open())
    {
        qDebug()<<("Failed to connection");
        return false;
    }
    else
    {
        qDebug()<<("Connected...");
        return true;
    }
}

bool DbManager::close_connection()
{
    mydb.close();
    qDebug()<<("Closing connection...");
    return true;
}

