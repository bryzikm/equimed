#ifndef USERSERVICES_H
#define USERSERVICES_H
#include <QString>
#include "dbmanager.h"
#include "csingleton.h"
#include "mainwindow.h"
#include "userlogin.h"

class UserServices
{
public:
    UserServices();
    QString findBranch(int id_branch);
    static QList<QString> findAllUser();

    QSqlQuery qry;

};

#endif // USERSERVICES_H
