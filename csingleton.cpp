#include "csingleton.h"

CSingleton::CSingleton()
{

}

int CSingleton::UserAuthorization(QString login, QString password)
{
    int check;
    login=login.trimmed();
    password=password.trimmed();
    qry.prepare("select * from user where login='"+login+"' and password='"+password+"'");
    if(qry.exec()){
        check=0;
        while(qry.next())
        {
            check++;
            setLogin(login);
        }
    }
    return check;
}

QString CSingleton::CheckRole()
{
    QString role;
    qry.prepare("select role from user where login='"+login+"'");
    if(qry.exec()){
        while(qry.next())
        {
            role=qry.value(0).toString();
            setRole(role);
            findAll();
        }
    }
    return role;
}

void CSingleton::findAll()
{
    qry.prepare("select * from user where login='"+login+"'");
    if(qry.exec()){
        while(qry.next())
        {
            setName(qry.value("name").toString());
            setSurname(qry.value("surname").toString());
            setPesel(qry.value("pesel").toString());
            setPhone(qry.value("phone").toInt());
            setEmail(qry.value("email").toString());
            setIdBranch(qry.value("id_branch").toInt());
        }
    }

}

CSingleton *CSingleton::getInstance()
{
    static CSingleton instance;
    return &instance;
}

void CSingleton::setLogin(QString login)
{
    this->login=login;
}

QString CSingleton::getLogin()
{
    return login;
}

void CSingleton::setRole(QString role)
{
    userRole=role;
}

QString CSingleton::getRole()
{
    return userRole;
}

void CSingleton::setName(QString name){
    this->name=name;
}

QString CSingleton::getName(){
    return name;
}

void CSingleton::setSurname(QString surname){
    this->surname=surname;
}

QString CSingleton::getSurname(){
    return surname;
}

void CSingleton::setEmail(QString email){
    this->email=email;
}

QString CSingleton::getEmail(){
    return email;
}

void CSingleton::setPhone(int phone){
    this->phone=phone;
}

int CSingleton::getPhone(){
    return phone;
}

void CSingleton::setIdBranch(int id_branch){
    this->id_branch=id_branch;
}

int CSingleton::getIdBranch(){
    return id_branch;
}

void CSingleton::setPesel(QString pesel){
    this->pesel=pesel;
}

QString CSingleton::getPesel(){
    return pesel;
}

