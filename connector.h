#ifndef CONNECTOR_H
#define CONNECTOR_H


class Connector
{
public:
    Connector();

    virtual void connect_with_database() = 0;
};

#endif // CONNECTOR_H
