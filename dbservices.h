#ifndef DBSERVICES_H
#define DBSERVICES_H
#include "userlogin.h"
#include "dbmanager.h"
#include "connector.h"

class DbServices : public Connector
{
public:
    DbServices();
    void connect_with_database();
};

#endif // DBSERVICES_H
