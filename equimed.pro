#-------------------------------------------------
#
# Project created by QtCreator 2016-03-20T10:35:20
#
#-------------------------------------------------

QT       += core gui sql widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = equimed
TEMPLATE = app

QMAKE_CXXFLAGS -= -std=c++14
@CONFIG += c++14@

SOURCES += main.cpp\
        mainwindow.cpp \
    userlogin.cpp \
    dbmanager.cpp \
    userservices.cpp \
    dbservices.cpp \
    usermenu.cpp \
    csingleton.cpp \
    userdatawidget.cpp \
    equipment/service/equipmentservice.cpp \
    equipment/form/newequipmentform.cpp \
    equipment/mapping/formmapping.cpp \
    hospitalWard/hospitalwardservice.cpp \
    user/user.cpp \
    user/usermanager.cpp \
    user/userquery.cpp \
    user/userconstants.cpp \
    user/userform.cpp \
    admin/userlist.cpp \
    equipment/equiplist.cpp \
    equipment/loanequiplist.cpp \
    equipment/stocktakerequiplist.cpp \
    equipment/searcher/equipmentsearcher.cpp \
    equipment/renderer/listrenderer.cpp \
    menu/menuabstractfactory.cpp \
    menu/adminmenufactory.cpp \
    menu/doctormenufactory.cpp \
    menu/stocktakermenufactory.cpp \
    menu/servicemanmenufactory.cpp \
    equipment/form/loanequipmentform.cpp \
    equipment/service/loanservice.cpp \
    equipment/damageequiplist.cpp \
    equipment/mapping/repairformmapping.cpp \
    equipment/form/repairform.cpp \
    user/createuserexception.cpp \
    connector.cpp \
    dbproxy.cpp
HEADERS  += mainwindow.h \
    userlogin.h \
    dbmanager.h \
    userservices.h \
    dbservices.h \
    usermenu.h \
    csingleton.h \
    userdatawidget.h \
    equipment/service/equipmentservice.h \
    equipment/form/newequipmentform.h \
    equipment/mapping/formmapping.h \
    hospitalWard/hospitalwardservice.h \
    user/user.h \
    user/usermanager.h \
    user/userquery.h \
    user/userconstants.h \
    user/userform.h \
    admin/userlist.h \
    equipment/equiplist.h \
    equipment/loanequiplist.h \
    equipment/stocktakerequiplist.h \
    equipment/searcher/equipmentsearcher.h \
    equipment/renderer/listrenderer.h \
    menu/menuabstractfactory.h \
    menu/adminmenufactory.h \
    menu/doctormenufactory.h \
    menu/stocktakermenufactory.h \
    menu/servicemanmenufactory.h \
    equipment/form/loanequipmentform.h \
    equipment/service/loanservice.h \
    equipment/damageequiplist.h \
    equipment/mapping/repairformmapping.h \
    equipment/form/repairform.h \
    branch/branchmanager.h \
    user/createuserexception.h \
    connector.h \
    dbproxy.h

FORMS    += mainwindow.ui \
    userlogin.ui \
    equipment\form\newequipmentform.ui \
    userdatawidget.ui \
    user/userform.ui \
    admin/userlist.ui \
    equipment/form/equipmentlist.ui \
    equipment/loanequiplist.ui \
    equipment/stocktakerequiplist.ui \
    equipment/searcher/equipmentsearcher.ui \
    equipment/form/loanequipmentform.ui \
    equipment/damageequiplist.ui \
    equipment/form/repairform.ui

RESOURCES += \
    images.qrc
