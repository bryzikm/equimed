#ifndef SERVICEMANMENUFACTORY_H
#define SERVICEMANMENUFACTORY_H

#include "menuabstractfactory.h"

class ServicemanMenuFactory : public MenuAbstractFactory
{
public:
    ServicemanMenuFactory(QMainWindow *mainwindow);
    QActionGroup* getMenu();
};

#endif // SERVICEMANMENUFACTORY_H
