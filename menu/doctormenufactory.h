#ifndef DOCTORMENUFACTORY_H
#define DOCTORMENUFACTORY_H

#include "menuabstractfactory.h"

class DoctorMenuFactory : public MenuAbstractFactory
{
public:
    DoctorMenuFactory(QMainWindow *mainwindow);
    QActionGroup* getMenu();
};

#endif // DOCTORMENUFACTORY_H
