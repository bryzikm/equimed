#include "doctormenufactory.h"
#include "menuabstractfactory.h"

#include <mainwindow.h>
DoctorMenuFactory::DoctorMenuFactory(QMainWindow *mainwindow) : MenuAbstractFactory(mainwindow)
{
    list<<"Strona główna"<<"Lista sprzętu"<<"Lista wypożyczonego sprzętu"<<"Wyszukiwarka"<<"Wyloguj";
    actlist<<"mainPage"<<"listOfEquipment"<<"listOfLoanEquipment"<<"serchEquipment"<<"logout";
}

QActionGroup* DoctorMenuFactory::getMenu()
{
    return MenuAbstractFactory::prepareMenu();
}
