#include "menuabstractfactory.h"
#include <mainwindow.h>
#include <QActionGroup>

MenuAbstractFactory::MenuAbstractFactory(QMainWindow *mainwindow) :  mainwindow(mainwindow)
{

}


QActionGroup* MenuAbstractFactory::prepareMenu() {
    QActionGroup* actionGroup = new QActionGroup(this->mainwindow);
    actionGroup->setExclusive(false);

    for(int i = 0; i < list.size(); i++)
    {
        QAction* action = this->prepareAction(actlist[i],list[i]);
        actionGroup->addAction(action);
    }

    return actionGroup;
}


QAction* MenuAbstractFactory::prepareAction(QString name, QString text)
{
    QAction* retact = new QAction(text,this->mainwindow);
    retact->setObjectName(name);
    retact->setEnabled(true);
    return retact;
}
