#include "servicemanmenufactory.h"
#include "menuabstractfactory.h"

#include <mainwindow.h>

ServicemanMenuFactory::ServicemanMenuFactory(QMainWindow *mainwindow) : MenuAbstractFactory(mainwindow)
{
    list<<"Strona główna"<<"Uszkodzony sprzęt"<<"Wyszukiwarka"<<"Wyloguj";
    actlist<<"mainPage"<<"damageEquipmentList"<<"serchEquipment"<<"logout";
}

QActionGroup* ServicemanMenuFactory::getMenu()
{
    return MenuAbstractFactory::prepareMenu();
}
