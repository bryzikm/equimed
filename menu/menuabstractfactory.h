#ifndef MENUABSTRACTFACTORY_H
#define MENUABSTRACTFACTORY_H

#include <mainwindow.h>
#include <QActionGroup>


class MenuAbstractFactory
{
public:
    MenuAbstractFactory(QMainWindow *mainwindow);
    virtual QActionGroup* getMenu() = 0;

protected:
    QMainWindow *mainwindow;

    QList<QString> list;
    QList<QString> actlist;

    QActionGroup* prepareMenu();
    QAction* prepareAction(QString name, QString text);
};

#endif // MENUABSTRACTFACTORY_H
