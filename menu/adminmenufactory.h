#ifndef ADMINMENUFACTORY_H
#define ADMINMENUFACTORY_H

#include "menuabstractfactory.h"
#include <mainwindow.h>

class AdminMenuFactory : public MenuAbstractFactory
{
public:
    AdminMenuFactory(QMainWindow *mainwindow);
    QActionGroup* getMenu();
};

#endif // ADMINMENUFACTORY_H
