#include "adminmenufactory.h"
#include "menuabstractfactory.h"

#include <mainwindow.h>

AdminMenuFactory::AdminMenuFactory(QMainWindow *mainwindow) : MenuAbstractFactory(mainwindow)
{
    list<<"Strona główna"<<"Wszyscy użytkownicy"<<"Dodaj użytkownika"<<"Wyloguj";
    actlist<<"mainPage"<<"userList"<<"addUser"<<"logout";
}

QActionGroup* AdminMenuFactory::getMenu()
{
    return MenuAbstractFactory::prepareMenu();
}
