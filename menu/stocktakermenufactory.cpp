#include "stocktakermenufactory.h"
#include "menuabstractfactory.h"
#include <mainwindow.h>

StocktakerMenuFactory::StocktakerMenuFactory(QMainWindow *mainwindow) : MenuAbstractFactory(mainwindow)
{
    list<<"Strona główna"<<"Lista sprzętu"<<"Dodaj nowy sprzęt"<<"Wyszukiwarka"<<"Wyloguj";
    actlist<<"mainPage"<<"listOfEquipment"<<"addEquipment"<<"serchEquipment"<<"logout";
}

QActionGroup* StocktakerMenuFactory::getMenu()
{
    return MenuAbstractFactory::prepareMenu();
}
