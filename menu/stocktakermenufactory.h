#ifndef STOCKTAKERMENUFACTORY_H
#define STOCKTAKERMENUFACTORY_H
#include "menuabstractfactory.h"
#include "menuabstractfactory.h"

class StocktakerMenuFactory : public MenuAbstractFactory
{
public:
    StocktakerMenuFactory(QMainWindow *mainwindow);
    QActionGroup* getMenu();
};

#endif // STOCKTAKERMENUFACTORY_H
