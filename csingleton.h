#ifndef CSINGLETON_H
#define CSINGLETON_H
#include <QString>
#include "dbmanager.h"
#include "dbservices.h"


class CSingleton
{
private:
    CSingleton();
    CSingleton( const CSingleton & );
    void findAll();
    QSqlQuery qry;
    QString login;
    QString userRole;

    QString name;
    QString surname;
    QString email;
    int phone;
    int id_branch;
    QString pesel;

protected:
static CSingleton *instance;

public:
    static CSingleton * getInstance();
    static CSingleton * deleteInstance();


    int UserAuthorization(QString login, QString password);
    QString CheckRole();

    void setLogin(QString login);
    QString getLogin();

    void setRole(QString role);
    QString getRole();

    void setName(QString name);
    QString getName();

    void setSurname(QString surname);
    QString getSurname();

    void setEmail(QString email);
    QString getEmail();

    void setPhone(int phone);
    int getPhone();

    void setIdBranch(int id_branch);
    int getIdBranch();

    void setPesel(QString pesel);
    QString getPesel();
};

#endif // CSINGLETON_H
