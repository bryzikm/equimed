#ifndef USERDATAWIDGET_H
#define USERDATAWIDGET_H

#include <QWidget>
#include "dbmanager.h"

namespace Ui {
class UserDataWidget;
}

class UserDataWidget : public QWidget
{
    Q_OBJECT

public:
    explicit UserDataWidget(QWidget *parent = 0);
    ~UserDataWidget();

private:
    Ui::UserDataWidget *ui;
    DbManager *dbManager;
};

#endif // USERDATAWIDGET_H
