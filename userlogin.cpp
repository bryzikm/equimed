#include "userlogin.h"
#include "ui_userlogin.h"
#include "dbproxy.h"
#include "connector.h"

userLogin::userLogin(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::userLogin)
{
    ui->setupUi(this);

    //Background
    QPixmap bg(":/images/medical_bg1.jpg");
    bg = bg.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Background, bg);
    this->setPalette(palette);

    std::unique_ptr<Connector> connector(new DbProxy);
    connector->connect_with_database();

    ui->label_pic->setText("<font color='white'>EQUIMED</font>");
    ui->label_2->setText("<font color='white'>Login</font>");
    ui->label_3->setText("<font color='white'>Hasło</font>");



}

userLogin::~userLogin()
{
    delete ui;
}

void userLogin::on_pushButtonLogin_clicked()
{
    login=ui->lineEdit_username->text();
    password=ui->lineEdit_password->text();

    int check=CSingleton::getInstance()->UserAuthorization(login,password);
    if(check==1){
        MainWindow *mainWindow = new MainWindow();
        mainWindow->mainParentForm = this;
        this->hide();
        this->ui->lineEdit_username->clear();
        this->ui->lineEdit_password->clear();
        mainWindow->show();
    }
    if(check<1){
        ui->labelMessageLogin->setText("Username and password is not correct!");
    }

}
