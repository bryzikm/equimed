#include "loanequiplist.h"
#include "ui_loanequiplist.h"
#include "csingleton.h"
#include <equipment/service/equipmentservice.h>

#include <QTableWidget>
#include <QTableWidgetItem>
#include <QString>
#include <QtSql>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QMessageBox>
#include <usermenu.h>
#include <memory>


void LoanEquipList::set_return_button(int j, QTableWidgetItem* returnEquip)
{
    ui->tableWidget->setIconSize(QSize(100,40));
    returnEquip->setSizeHint(QSize(100, 40));
    returnEquip->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    QPixmap pixmap(":/images/button-return.png");
    QIcon qicon(pixmap);
    returnEquip->setIcon(qicon);
    returnEquip->setText(equip.at(j+5));
}

int LoanEquipList::return_button_show_message(QTableWidgetItem* item)
{
    QMessageBox message_box;
    QString message = item->text() + " zostanie oddany, jesteś pewien?";
    message_box.setText(message);
    message_box.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
    return message_box.exec();
}

void LoanEquipList::confirm_return_action(QTableWidgetItem* item)
{
    if(return_button_show_message(item) == QMessageBox::Yes)
    {       
        std::unique_ptr<EquipmentService> equip_service(new EquipmentService);
        bool returnEquip = equip_service->updateLoan(item->text());
        if(returnEquip){
            QMessageBox::information(this, "Return", "Sprzet został zwrócony");
            LoanEquipList::showLoanEquip();
        }
        else
            QMessageBox::critical(this, "Return", "Wystąpił błąd!");
    }
}

void LoanEquipList::return_button_clicked(QTableWidgetItem* item)
{
    int return_column=7;
    for (int i=0; i<get_row_quantity(); i++)
        if (ui->tableWidget->item(i, return_column) == item)
            confirm_return_action(item);
}

void LoanEquipList::connect_return_button()
{
    connect(ui->tableWidget, SIGNAL(itemClicked(QTableWidgetItem*)),
         this, SLOT(return_button_clicked(QTableWidgetItem*)));
}

int LoanEquipList::get_row_quantity()
{
    return (equip.size()/dataOfLoanEquip);
}

void LoanEquipList::set_id_table_item(int j, QTableWidgetItem *id)
{
    std::unique_ptr<EquipmentService> es(new EquipmentService);
    id->setText(equip.at(j+5));
    id->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    id->setToolTip(es->generatePath(id));
}

void LoanEquipList::set_name_table_item(int j, QTableWidgetItem *name, QTableWidgetItem *id)
{
    std::unique_ptr<EquipmentService> es(new EquipmentService);
    QString equipName=es->findName((equip.at(j+5)).toInt());
    name->setText(equipName);
    name->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    name->setToolTip(es->generatePath(id));
}

void LoanEquipList::showLoanEquip()
{
    equip = EquipmentService::loanEquipList();

    ui->tableWidget->setRowCount(get_row_quantity());
    ui->tableWidget->setColumnWidth(0, 60);
    ui->tableWidget->setColumnWidth(1, 130);
    ui->tableWidget->setColumnWidth(3, 200);
    ui->tableWidget->setColumnWidth(4, 180);
    ui->tableWidget->setColumnWidth(5, 150);
    ui->tableWidget->setColumnWidth(6, 150);
    ui->tableWidget->setColumnWidth(7, 102);

    std::unique_ptr<UserServices> us(new UserServices);

    int j=0;
    while(j<equip.size()){
        for(int i=0; i<get_row_quantity(); i++){
            ui->tableWidget->setRowHeight(i, 60);

            QTableWidgetItem* id = new QTableWidgetItem();
            QTableWidgetItem* name = new QTableWidgetItem();
            QTableWidgetItem* id_wyp = new QTableWidgetItem();
            QTableWidgetItem* branchName_l = new QTableWidgetItem();
            QTableWidgetItem* branchName_g = new QTableWidgetItem();
            QTableWidgetItem* date_s = new QTableWidgetItem();
            QTableWidgetItem* date_f = new QTableWidgetItem();
            QTableWidgetItem* returnEquip = new QTableWidgetItem();

            id_wyp->setText(equip.at(j));

            QString branch_l=us->findBranch((equip.at(j+1)).toInt());
            branchName_l->setText(branch_l);
            QString branch_g=us->findBranch((equip.at(j+2)).toInt());
            branchName_g->setText(branch_g);
            date_s->setText(equip.at(j+3));
            date_f->setText(equip.at(j+4));
            set_return_button(j,returnEquip);

            set_id_table_item(j,id);
            set_name_table_item(j,name,id);

            ui->tableWidget->setItem(i,0,id);
            ui->tableWidget->setItem(i,1,name);
            ui->tableWidget->setItem(i,2,id_wyp);
            ui->tableWidget->setItem(i,3,branchName_l);
            ui->tableWidget->setItem(i,4,branchName_g);
            ui->tableWidget->setItem(i,5,date_s);
            ui->tableWidget->setItem(i,6,date_f);
            ui->tableWidget->setItem(i,7,returnEquip);

            j=j+6;
        }
    }
    connect_return_button();
}

LoanEquipList::LoanEquipList(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LoanEquipList)
{
    ui->setupUi(this);
    showLoanEquip();
}

LoanEquipList::~LoanEquipList()
{
    delete ui;
}
