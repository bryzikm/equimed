#ifndef EQUIPLIST_H
#define EQUIPLIST_H

#include <QWidget>
#include "dbmanager.h"

namespace Ui {
class EquipList;
}

class  EquipList : public QWidget
{
    Q_OBJECT

public:
    explicit EquipList(QWidget *parent = 0);
    ~EquipList();

private:
    Ui::EquipList *ui;
    DbManager *dbManager;
};

#endif // EQUIPLIST_H
