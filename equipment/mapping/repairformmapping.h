#ifndef REPAIRFORMMAPPING_H
#define REPAIRFORMMAPPING_H

#include <QString>

class RepairFormMapping
{
public:
    static const QString SERIAL;
    static const QString DESCRIPTION;
    static const QString DATE;
    static const QString EQUIPMENT_ID;

private:
    RepairFormMapping();
};

#endif // REPAIRFORMMAPPING_H
