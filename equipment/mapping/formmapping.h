#ifndef FORMMAPPING_H
#define FORMMAPPING_H
#include <QString>

class FormMapping
{
public:
    /*Form input names*/
    static const QString NAME;
    static const QString PRODUCER;
    static const QString INVENTORY_NUMBER;
    static const QString PURCHASE_DATE;
    static const QString ADDING_DATE;
    static const QString GUARANTEE_DATE;
    static const QString OVERVIEW_DATE;
    static const QString HOSPITAL_WARD;
private:
    FormMapping();
};

#endif // FORMMAPPING_H
