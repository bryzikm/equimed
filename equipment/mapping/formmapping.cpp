#include "formmapping.h"

FormMapping::FormMapping()
{

}

const QString FormMapping::NAME = "name";
const QString FormMapping::PRODUCER = "producer";
const QString FormMapping::INVENTORY_NUMBER = "inventoryNumber";
const QString FormMapping::PURCHASE_DATE = "purchaseDate";
const QString FormMapping::ADDING_DATE = "addingDate";
const QString FormMapping::GUARANTEE_DATE = "guaranteeDate";
const QString FormMapping::OVERVIEW_DATE = "overviewDate";
const QString FormMapping::HOSPITAL_WARD = "hospitalWard";
