#ifndef DAMAGEEQUIPLIST_H
#define DAMAGEEQUIPLIST_H

#include <QWidget>
#include "equipment/service/equipmentservice.h"

namespace Ui {
class DamageEquipList;
}

class DamageEquipList : public QWidget
{
    Q_OBJECT

public:
    explicit DamageEquipList(QWidget *parent = 0);

    void renderDamageEquipList();
    void setRepairButton(QTableWidgetItem *item, QString equipmentId);
    void connectRepairButton();

    void setEditButton(int i, QTableWidgetItem* editButton);
    void connectEditButton();

    void confirm_edit_action(QTableWidgetItem *item);

    QList<QString> get_value_from_row(QTableWidgetItem* item);

    void set_id_table_item(int j, QTableWidgetItem* id);

    void set_name_table_item(int j, QTableWidgetItem* name, QTableWidgetItem *id);

    void set_producent_table_item(int j, QTableWidgetItem* producent);

    void set_nr_inw_table_item(int j, QTableWidgetItem* nr_inw);

    void set_purchase_table_item(int j, QTableWidgetItem* purchase);

    void set_introduced_table_item(int j, QTableWidgetItem* introduced);

    void set_inspection_table_item(int j, QTableWidgetItem* inspection);

    void set_branchName_table_item(int j, QTableWidgetItem* branchName);

    ~DamageEquipList();

public slots:
    void editButtonClicked(QTableWidgetItem *item);
    void repairButtonClicked(QTableWidgetItem *item);

private:
    Ui::DamageEquipList *ui;
    QList<QString> damageEquip;
    const int valueOfEquip = 8;
    const int EDIT_BUTTON_COLUMN_NUMBER = 8;
    const int REPAIR_BUTTON_COLUMN_NUMBER = 9;
    const int QUANTITY_OF_VISIBLE_TABLE_VALUE = 7;
    const int ICON_WIDTH = 100;
    const int ICON_HEIGHT = 40;
    const QString TOOL_TIP_ON_BLOCKED_FIELD = "To pole nie może zostać zmienione.";
    const QString TOOL_TIP_ON_EDITABLE_FIELD = "To pole może zostać zmienione.";
    int getRowQuantity();

    QString findBranch(int id);
};

#endif // DAMAGEEQUIPLIST_H
