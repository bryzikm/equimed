#include "repairform.h"
#include "ui_repairform.h"

#include <QTableWidgetItem>
#include <QMessageBox>
#include <equipment/mapping/repairformmapping.h>
#include <equipment/service/equipmentservice.h>
#include <memory>

RepairForm::RepairForm(QWidget *parent, QTableWidgetItem *item) :
    QDialog(parent),
    ui(new Ui::RepairForm)
{
    ui->setupUi(this);
    this->item = item;
}

RepairForm::~RepairForm()
{
    delete ui;
}

void RepairForm::on_save_clicked()
{
    std::unique_ptr<EquipmentService> service(new EquipmentService);
    bool damage = service->updateDamage(this->item->text());
    bool repair = service->reportRepair(this->getFormValues());

    if(damage && repair)
    {
        QMessageBox::information(this, "Naprawa", "Zgłoszono naprawę urządzenia!");
        this->close();
    }
    else
    {
        QMessageBox::critical(this, "Błąd", "Wypełnij wszystkie pola!");
    }
}

void RepairForm::on_close_clicked()
{
    this->close();
}

QHash<QString, QString> RepairForm::getFormValues() {
    QHash<QString, QString> formValues;

    formValues[RepairFormMapping::SERIAL] = this->ui->serial->text();
    formValues[RepairFormMapping::DESCRIPTION] = this->ui->description->toPlainText();
    formValues[RepairFormMapping::DATE] = this->ui->date->text();
    formValues[RepairFormMapping::EQUIPMENT_ID] = this->item->text();

    return formValues;
}
