#include "newequipmentform.h"
#include "ui_newequipmentform.h"
#include "hospitalWard/hospitalwardservice.h"
#include "equipment/service/equipmentservice.h"
#include "equipment/mapping/formmapping.h"
#include <memory>

NewDeviceForm::NewDeviceForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewEquipmentForm)
{
    ui->setupUi(this);
    this->setupHospitalWards();
}

void NewDeviceForm::setupHospitalWards() {

    std::unique_ptr<HospitalWardService> hospitalWardService(new HospitalWardService);

    QMap<int, QString> hospitalWards = hospitalWardService->getAllWards();

    for(auto row : hospitalWards.toStdMap()) {
        this->ui->hostpitalWard->addItem(row.second, QVariant(row.first));
    }

}

/* Save device */
void NewDeviceForm::on_save_released()
{
    std::unique_ptr<EquipmentService> service(new EquipmentService);
    bool result = service->addNewDevice(this->getFormValues());

    if(result) {
        this->ui->resultMessage->setText("Nowe urządzenie zostało dodane");
    } else {
        this->ui->resultMessage->setText("Wypełnij wszystkie pola!");
    }
}

/* Prepare values from form*/
QHash<QString, QString> NewDeviceForm::getFormValues()
{

    QHash<QString, QString> formValues;

    formValues[FormMapping::NAME] = this->ui->name->text();
    formValues[FormMapping::PRODUCER] = this->ui->producer->text();
    formValues[FormMapping::INVENTORY_NUMBER] = this->ui->inventoryNumber->text();
    formValues[FormMapping::PURCHASE_DATE] = this->ui->purchaseDate->text();
    formValues[FormMapping::ADDING_DATE] = this->ui->addingDate->text();
    formValues[FormMapping::GUARANTEE_DATE] = this->ui->guaranteeDate->text();
    formValues[FormMapping::OVERVIEW_DATE] = this->ui->overviewDate->text();

    int index = this->ui->hostpitalWard->currentIndex();
    formValues[FormMapping::HOSPITAL_WARD] = this->ui->hostpitalWard->itemData(index).toString();

    return formValues;
}

/* Close dialog window */
void NewDeviceForm::on_close_released()
{
    this->close();
}

/* Reset form inputs */
void NewDeviceForm::on_reset_released()
{
    foreach(QLineEdit *widget, this->findChildren<QLineEdit*>()) {
        widget->clear();
    }
}

NewDeviceForm::~NewDeviceForm()
{
    delete ui;
}
