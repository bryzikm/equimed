#ifndef REPAIRFORM_H
#define REPAIRFORM_H

#include <QDialog>
#include <QTableWidgetItem>

namespace Ui {
class RepairForm;
}

class RepairForm : public QDialog
{
    Q_OBJECT

public:
    explicit RepairForm(QWidget *parent = 0, QTableWidgetItem *item = 0);
    ~RepairForm();

private slots:
    void on_save_clicked();

    void on_close_clicked();

private:
    QHash<QString, QString> getFormValues();
    Ui::RepairForm *ui;
    QTableWidgetItem *item;
};

#endif // REPAIRFORM_H
