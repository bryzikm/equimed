#include "loanequipmentform.h"
#include "ui_loanequipmentform.h"

#include "hospitalWard/hospitalwardservice.h"
#include "equipment/service/equipmentservice.h"
#include "equipment/service/loanservice.h"

#include <QMessageBox>
#include <memory>

LoanEquipmentForm::LoanEquipmentForm(QWidget *parent, int equipmentId) :
    QDialog(parent), equipmentId(equipmentId),
    ui(new Ui::LoanEquipmentForm)
{

    ui->setupUi(this);
    this->setupForm();
}

void LoanEquipmentForm::setupForm() {
    this->setupHospitalWards();
    this->setupDates();
    this->setupEquipmentInfo();
}


void LoanEquipmentForm::setupHospitalWards() {

    std::unique_ptr<HospitalWardService> hospitalWardService(new HospitalWardService);
    QMap<int, QString> hospitalWards = hospitalWardService->getAllWards();

    for(auto row : hospitalWards.toStdMap()) {
        this->ui->lenderWard->addItem(row.second, QVariant(row.first));
    }
}

void LoanEquipmentForm::setupDates() {
    QDate date = QDate::currentDate();
    this->ui->lendDate->setDate(date);
    this->ui->returnDate->setDate(date.addDays(1));
}

void LoanEquipmentForm::setupEquipmentInfo()
{
    std::unique_ptr<EquipmentService> equipmentService(new EquipmentService);
    QList<QString> equipmentInfo = equipmentService->findEquipmentById(this->equipmentId);

    this->ui->equipmentId->setText(equipmentInfo.at(0));
    this->ui->equipmentName->setText(equipmentInfo.at(1));
    this->ui->equipmentOwnerWard->setText(equipmentInfo.at(7));
}

LoanEquipmentForm::~LoanEquipmentForm()
{
    delete ui;
}

void LoanEquipmentForm::on_buttonBox_accepted()
{
    int equipmentId = this->ui->equipmentId->text().toInt();
    QDate lendDate = this->ui->lendDate->date();
    QDate returnDate = this->ui->returnDate->date();

    if(returnDate < lendDate) {
        QMessageBox::critical(this, "Wypożyczenie", "Sprzęt nie może zostać wypożyczony - błędna data zwrotu");
    }

    int index = this->ui->lenderWard->currentIndex();
    int lenderWardId = this->ui->lenderWard->itemData(index).toInt();

    std::unique_ptr<LoanService> loanService(new LoanService);

    if(loanService->addNewLoan(equipmentId, lendDate, returnDate, lenderWardId)) {
        QMessageBox::information(this, "Wypożyczenie", "Sprzęt został pomyślnie wypożyczony");
    } else {
        QMessageBox::critical(this, "Wypożyczenie", "Sprzęt nie może zostać wypożyczony");
    }
}
