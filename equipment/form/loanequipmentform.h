#ifndef LOANEQUIPMENTFORM_H
#define LOANEQUIPMENTFORM_H

#include <QDialog>

namespace Ui {
class LoanEquipmentForm;
}

class LoanEquipmentForm : public QDialog
{
    Q_OBJECT

public:
    explicit LoanEquipmentForm(QWidget *parent = 0, int equipmentId = 0);
    ~LoanEquipmentForm();


private slots:
    void on_buttonBox_accepted();

private:
    int equipmentId;

    Ui::LoanEquipmentForm *ui;
    void setupForm();
    void setupDates();
    void setupHospitalWards();
    void setupEquipmentInfo();
};

#endif // LOANEQUIPMENTFORM_H
