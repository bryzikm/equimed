#ifndef NEWEQUIPMENTFORM_H
#define NEWEQUIPMENTFORM_H

#include <QDialog>

namespace Ui {
class NewEquipmentForm;
}

class NewDeviceForm : public QDialog
{
    Q_OBJECT

public:
    explicit NewDeviceForm(QWidget *parent = 0);
    ~NewDeviceForm();

private slots:
    void on_save_released();

    void on_close_released();

    void on_reset_released();

private:
    QHash<QString, QString> getFormValues();
    void setupHospitalWards();
    Ui::NewEquipmentForm *ui;
};

#endif // NEWEQUIPMENTFORM_H
