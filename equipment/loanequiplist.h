#ifndef LOANEQUIPLIST_H
#define LOANEQUIPLIST_H

#include <QWidget>
#include "dbmanager.h"
#include "equipment/service/equipmentservice.h"

namespace Ui {
class LoanEquipList;
}

class LoanEquipList : public QWidget
{
    Q_OBJECT

public:
    explicit LoanEquipList(QWidget *parent = 0);
    void showLoanEquip();
    void set_return_button(int j, QTableWidgetItem* returnEquip);
    void connect_return_button();
    void confirm_return_action(QTableWidgetItem* item);
    int return_button_show_message(QTableWidgetItem* item);
    int get_row_quantity();
    void set_id_table_item(int j, QTableWidgetItem* id);
    void set_name_table_item(int j, QTableWidgetItem* name, QTableWidgetItem *id);
    ~LoanEquipList();

public slots:
    void return_button_clicked(QTableWidgetItem *item);

private:
    Ui::LoanEquipList *ui;
    DbManager *dbManager;
    QList<QString> equip;
    int dataOfLoanEquip = 6;
};

#endif // LOANEQUIPLIST_H
