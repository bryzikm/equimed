#ifndef LOANSERVICE_H
#define LOANSERVICE_H

#include <QSqlQuery>
#include <QDate>

#include "dbmanager.h"

class LoanService
{
public:
    LoanService();
    bool addNewLoan(int equipmentId, QDate lendDate, QDate returnDate, int lenderWardId);
    ~LoanService();

private:
    DbManager *dbManager;
};

#endif // LOANSERVICE_H
