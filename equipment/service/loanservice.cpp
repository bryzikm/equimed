#include "loanservice.h"

#include <QDebug>
#include <QSqlQuery>
#include <QDate>

#include "dbmanager.h"

LoanService::LoanService()
{

}

bool LoanService::addNewLoan(int equipmentId, QDate lendDate, QDate returnDate, int lenderWardId)
{

    QSqlQuery qry;

    qry.prepare("SELECT wypozyczenie FROM sprzet WHERE nr_id = :nrId");
    qry.bindValue(":nrId", equipmentId);
    if(qry.exec()){
        qry.next();
        if(qry.value("wypozyczenie").toBool()) {
            return false;
        }

    }

    //add information into 'wypozyczenie'
    qry.prepare("INSERT INTO wypozyczenie (id_oddzialu_w, id_oddzialu_p, data_p, data_k, nr_id) "
                "VALUES ((SELECT id_oddzialu FROM sprzet WHERE nr_id = :nrId), :idOddzialuP, :dataP, :dataK, :nrId)");

    qry.bindValue(":idOddzialuP", lenderWardId);
    qry.bindValue(":dataP", lendDate);
    qry.bindValue(":dataK", returnDate);
    qry.bindValue(":nrId", equipmentId);

    if(qry.exec()){
        //update 'sprzet' row
        qry.prepare("UPDATE sprzet SET wypozyczenie = 1 WHERE nr_id = :nrId");
        qry.bindValue(":nrId", equipmentId);
        if(qry.exec()) return true;
    }

    return false;
}

LoanService::~LoanService()
{
}
