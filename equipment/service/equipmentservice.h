#ifndef EQUIPMENTSERVICE_H
#define EQUIPMENTSERVICE_H
#include "dbmanager.h"

#include <QString>
#include <QTableWidgetItem>
#include "csingleton.h"
#include "mainwindow.h"
#include "userlogin.h"

class EquipmentService
{
public:
    EquipmentService();
    bool addNewDevice(QHash<QString, QString> values);
    bool updateLoan(QString id);
    bool reportRepair(QHash<QString, QString> formValues);
    bool edit_equipment(QString id, QString nr_inw);
    bool edit_inspection(QString id, QString inspection);
    bool reportEquipmentDamage(QString equipmentId);
    bool updateDamage(QString id);
    static QList<QString> findAllEquip();
    static QList<QString> loanEquipList();
    static QList<QString> damageEquipList();
    QString findName(int id);
    QList<QString> findEquipmentById(int equipmentId);
    QList<QString> findEquipByPhrase(QString phrase);
    QString generatePath(QTableWidgetItem *id);
    ~EquipmentService();

private:
    DbManager *dbManager;
    bool isValid(const QHash<QString, QString> &values);
    bool save(const QHash<QString, QString> &values);
    bool saveRepair(const QHash<QString, QString> &values);

};

#endif // EQUIPMENTSERVICE_H
