#include "equipmentservice.h"
#include "dbmanager.h"
#include "equipment/form/newequipmentform.h"
#include "equipment/mapping/formmapping.h"
#include "csingleton.h"

#include <equipment/mapping/repairformmapping.h>
#include <QDebug>
#include <QList>
#include <QTableWidgetItem>

EquipmentService::EquipmentService()
{

}

bool EquipmentService::addNewDevice(QHash<QString, QString> values)
{
    if(this->isValid(values)) {
        this->save(values);
        return true;
    }

    return false;
}

bool EquipmentService::reportRepair(QHash<QString, QString> formValues) {

    if(this->isValid(formValues)) {

        return this->saveRepair(formValues);
    }

    return false;
}

bool EquipmentService::updateDamage(QString id) {

    QSqlQuery qry;

    QString update = "UPDATE sprzet SET awaria = '0' where nr_id = '" + id + "'";

    qry.prepare(update);

    if(qry.exec())
    {
        return true;
    }

    return false;
}

bool EquipmentService::saveRepair(const QHash<QString, QString> &values)
{
    QSqlQuery qry;
    qry.prepare("INSERT INTO naprawa (nr_sr, opis, data, nr_id) "
                "VALUES (:serial, :description, :date, :id)");

    qry.bindValue(":serial", values[RepairFormMapping::SERIAL]);
    qry.bindValue(":description", values[RepairFormMapping::DESCRIPTION]);
    qry.bindValue(":date", values[RepairFormMapping::DATE]);
    qry.bindValue(":id", values[RepairFormMapping::EQUIPMENT_ID]);

    if(qry.exec()){
        return true;
    }

    return false;
}

bool EquipmentService::isValid(const QHash<QString, QString> &values)
{
    foreach(QString value, values) {
        if(value.isEmpty()) {
            return false;
        }
    }

    return true;
}

QString EquipmentService::generatePath(QTableWidgetItem *id)
{
    QString path = "<img src=':/images/sprzet/"+id->text()+".png'>";

    return path;
}

bool EquipmentService::save(const QHash<QString, QString> &values)
{

    QSqlQuery qry;
    qry.prepare("INSERT INTO sprzet (nazwa, producent, nr_in, data_zakupu, dw, gwarancja, ostatni_przeglad, id_oddzialu) "
                "VALUES (:name, :producer, :inventoryNumber, :purchaseDate, :addingDate, :overviewDate, :guaranteeDate, :hospitalWard)");

    qry.bindValue(":name", values[FormMapping::NAME]);
    qry.bindValue(":producer", values[FormMapping::PRODUCER]);
    qry.bindValue(":inventoryNumber", values[FormMapping::INVENTORY_NUMBER]);
    qry.bindValue(":purchaseDate", values[FormMapping::PURCHASE_DATE]);
    qry.bindValue(":addingDate", values[FormMapping::ADDING_DATE]);
    qry.bindValue(":overviewDate", values[FormMapping::OVERVIEW_DATE]);
    qry.bindValue(":guaranteeDate", values[FormMapping::GUARANTEE_DATE]);
    qry.bindValue(":hospitalWard", values[FormMapping::HOSPITAL_WARD]);

    if(qry.exec()){
        return true;
    }

    return false;
}

QString EquipmentService::findName(int id)
{
    QString name;
    QSqlQuery qry;
    qry.prepare("select nazwa from sprzet where nr_id='"+QString::number(id)+"'");
    if(qry.exec()){
        while(qry.next())
            {
                name=qry.value(0).toString();
            }
     }
    return name;
}


QList<QString> EquipmentService::findEquipmentById(int equipmentId)
{
    QList<QString> result;

    QSqlQuery qry;
    qry.prepare("SELECT s.*, o.nazwa AS nazwa_oddzialu FROM sprzet s "
                "JOIN oddzial o ON o.id_oddzialu = s.id_oddzialu "
                "WHERE s.nr_id='" + QString::number(equipmentId) + "'");

    if(qry.exec()){
        while(qry.next()){
            result.append(qry.value("nr_id").toString());
            result.append(qry.value("nazwa").toString());
            result.append(qry.value("producent").toString());
            result.append(qry.value("nr_in").toString());
            result.append(qry.value("data_zakupu").toString());
            result.append(qry.value("dw").toString());
            result.append(qry.value("ostatni_przeglad").toString());
            result.append(qry.value("nazwa_oddzialu").toString());
        }
    }

    return result;
}
QList<QString> EquipmentService::loanEquipList()
{
    QList<QString> loanEquip;

    QSqlQuery qry;
    qry.prepare("select * from wypozyczenie");

    if(qry.exec()){
        while(qry.next()){
            loanEquip.append(qry.value("id_wyp").toString());
            loanEquip.append(qry.value("id_oddzialu_w").toString());
            loanEquip.append(qry.value("id_oddzialu_p").toString());
            loanEquip.append(qry.value("data_p").toString());
            loanEquip.append(qry.value("data_k").toString());
            loanEquip.append(qry.value("nr_id").toString());
        }
    }

    return loanEquip;
}

QList<QString> EquipmentService::damageEquipList() {
    QList<QString> damageEquip;

    QSqlQuery query;
    query.prepare("SELECT * FROM sprzet WHERE awaria = '1'");

    if(query.exec()) {
        while(query.next()) {
            damageEquip.append(query.value("nr_id").toString());
            damageEquip.append(query.value("nazwa").toString());
            damageEquip.append(query.value("producent").toString());
            damageEquip.append(query.value("nr_in").toString());
            damageEquip.append(query.value("data_zakupu").toString());
            damageEquip.append(query.value("dw").toString());
            damageEquip.append(query.value("ostatni_przeglad").toString());
            damageEquip.append(query.value("id_oddzialu").toString());
        }
    }

    return damageEquip;
}

bool EquipmentService::updateLoan(QString id)
{
    QSqlQuery qry;
    QSqlQuery qry_wyp;
    qDebug()<<id;
    QString update = "UPDATE sprzet SET wypozyczenie = '0' where nr_id = '" + id + "'";
    QString delete_wyp = "DELETE FROM wypozyczenie where nr_id = '" + id + "'";

    qry.prepare(update);
    qry_wyp.prepare(delete_wyp);

    if(qry.exec() && qry_wyp.exec())
    {
        qDebug()<<"Sprzęt został zwrócony";
        return true;
    }
    else {

        qDebug()<<qry.lastError();
    }

    return false;
}

bool EquipmentService::reportEquipmentDamage(QString equipmentId) {
    QSqlQuery damageQuery;
    QString damage = "UPDATE sprzet SET awaria = '1' WHERE nr_id = '" + equipmentId + "'";

    damageQuery.prepare(damage);

    if(damageQuery.exec())
        return true;
    else
        return false;
}

bool EquipmentService::edit_equipment(QString id, QString nr_inw)
{
    QSqlQuery qry;
    QString sql_request = "update sprzet set "
                          "nr_in = '"+nr_inw+"'"
                          "where nr_id = '"+id+"'";
    qry.prepare(sql_request);
    return qry.exec();
}

bool EquipmentService::edit_inspection(QString id, QString inspection)
{
    QSqlQuery qry;
    QString sql_request = "update sprzet set "
                          "ostatni_przeglad = '"+inspection+"'"
                          "where nr_id = '"+id+"'";
    qry.prepare(sql_request);
    return qry.exec();
}


EquipmentService::~EquipmentService() {

}

QList<QString> EquipmentService::findAllEquip(){

    QList<QString> equip;
    QSqlQuery qryList;

    qryList.prepare("select * from sprzet where awaria = '0' and wypozyczenie = '0'");
    if(qryList.exec()){
        while(qryList.next()){
            equip.append(qryList.value("nr_id").toString());
            equip.append(qryList.value("nazwa").toString());
            equip.append(qryList.value("producent").toString());
            equip.append(qryList.value("nr_in").toString());
            equip.append(qryList.value("data_zakupu").toString());
            equip.append(qryList.value("dw").toString());
            equip.append(qryList.value("ostatni_przeglad").toString());
            equip.append(qryList.value("id_oddzialu").toString());
        }
    }

    return equip;
}

QList<QString> EquipmentService::findEquipByPhrase(QString phrase) {

    QList<QString> equip;
    QSqlQuery qryList;

    qryList.prepare("SELECT * FROM sprzet WHERE nazwa LIKE '%" + phrase + "%' OR producent LIKE '%" + phrase + "%' OR nr_id LIKE '%" + phrase + "%'");
    if(qryList.exec()){
        while(qryList.next()){
            equip.append(qryList.value("nr_id").toString());
            equip.append(qryList.value("nazwa").toString());
            equip.append(qryList.value("producent").toString());
            equip.append(qryList.value("nr_in").toString());
            equip.append(qryList.value("data_zakupu").toString());
            equip.append(qryList.value("dw").toString());
            equip.append(qryList.value("ostatni_przeglad").toString());
            equip.append(qryList.value("id_oddzialu").toString());
        }
    }

    return equip;
}
