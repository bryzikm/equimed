#include "equipmentsearcher.h"
#include "ui_equipmentsearcher.h"
#include <QDebug>

#include "equipment/service/equipmentservice.h"
#include "equipment/renderer/listrenderer.h"
#include <memory>

EquipmentSearcher::EquipmentSearcher(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EquipmentSearcher)
{
    ui->setupUi(this);
}


EquipmentSearcher::~EquipmentSearcher()
{
    delete ui;
}

void EquipmentSearcher::on_searchButton_clicked()
{
    QString searchedPhrase = this->ui->searchPhrase->text();
    std::unique_ptr<EquipmentService> equipmentService(new EquipmentService);

    QList<QString> equip = equipmentService->findEquipByPhrase(searchedPhrase);
\
    ListRenderer::renderList(this, equip, ui->tableWidget);
}
