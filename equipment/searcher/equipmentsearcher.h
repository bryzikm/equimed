#ifndef EQUIPMENTSEARCHER_H
#define EQUIPMENTSEARCHER_H

#include <QWidget>

namespace Ui {
class EquipmentSearcher;
}

class EquipmentSearcher : public QWidget
{
    Q_OBJECT

public:
    explicit EquipmentSearcher(QWidget *parent = 0);
    ~EquipmentSearcher();

private slots:
    void on_searchButton_clicked();

private:
    Ui::EquipmentSearcher *ui;
};

#endif // EQUIPMENTSEARCHER_H
