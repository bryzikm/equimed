#include "stocktakerequiplist.h"
#include "ui_stocktakerequiplist.h"
#include "equipment/service/equipmentservice.h"
#include "csingleton.h"

#include <QTableWidget>
#include <QTableWidgetItem>
#include <QString>
#include <QtSql>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QMessageBox>
#include <memory>

int StockTakerEquipList::get_row_quantity()
{
    return (equip.size()/dataOfEquip);
}

QList<QString> StockTakerEquipList::get_value_from_row(QTableWidgetItem *item)
{
    QList<QString> equipment_field_value;
    for(int i = 0;i<QUANTITY_OF_VISIBLE_TABLE_VALUE;i++)
        equipment_field_value.append(ui->tableWidget->model()->data(
            ui->tableWidget->model()->index(item->text().toInt(), i)).toString());
    return equipment_field_value;
}

void StockTakerEquipList::set_edit_button(int i, QTableWidgetItem* editNrInw)
{
    ui->tableWidget->setIconSize(QSize(BUTTON_WIDTH, BUTTON_HEIGHT));
    editNrInw->setSizeHint(QSize(BUTTON_WIDTH, BUTTON_HEIGHT));
    editNrInw->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    QPixmap pixmap(":/images/button-edit.png");
    QIcon qicon(pixmap);
    editNrInw->setIcon(qicon);
    editNrInw->setText(QString::number(i));
}

void StockTakerEquipList::confirm_edit_equipment_action(QTableWidgetItem *item)
{
    QList<QString> equipment_field_value = get_value_from_row(item);
    std::unique_ptr<EquipmentService> equip_service(new EquipmentService);
    bool edited_equipment = equip_service->edit_equipment(equipment_field_value[0],
                                               equipment_field_value[3]);
    if(edited_equipment)
    {
        QMessageBox::information(this, "Edycja", "Numer inwentarzowy został zaktualizowany");
        StockTakerEquipList::get_equipment_data_from_database();
    }
    else
        QMessageBox::critical(this, "Edycja", "  Błąd!  ");

}

void StockTakerEquipList::edit_button_clicked(QTableWidgetItem *item)
{
    for (int i=0; i<get_row_quantity(); i++)
    {
        if (ui->tableWidget->item(i, StockTakerEquipList::EDIT_BUTTON_COLUMN_NUMBER) == item)
        {
            confirm_edit_equipment_action(item);
        }
    }
}

void StockTakerEquipList::connect_edit_equipment_button()
{
    connect(ui->tableWidget, SIGNAL(itemClicked(QTableWidgetItem*)),
            this, SLOT(edit_button_clicked(QTableWidgetItem*)));
}

void StockTakerEquipList::set_id_table_item(int j, QTableWidgetItem *id)
{
    std::unique_ptr<EquipmentService> es(new EquipmentService);
    id->setText(equip.at(j));
    id->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    id->setToolTip(es->generatePath(id));
}

void StockTakerEquipList::set_name_table_item(int j, QTableWidgetItem *name, QTableWidgetItem *id)
{
    std::unique_ptr<EquipmentService> es(new EquipmentService);
    name->setText(equip.at(j+1));
    name->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    name->setToolTip(es->generatePath(id));
}

void StockTakerEquipList::set_producent_table_item(int j, QTableWidgetItem *producent)
{
    producent->setText(equip.at(j+2));
    producent->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    producent->setToolTip(TOOL_TIP_ON_BLOCKED_FIELD);
}

void StockTakerEquipList::set_nr_inw_table_item(int j, QTableWidgetItem *nr_inw)
{
    nr_inw->setText(equip.at(j+3));
    nr_inw->setToolTip(TOOL_TIP_ON_EDITABLE_FIELD);
}

void StockTakerEquipList::set_purchase_table_item(int j, QTableWidgetItem *purchase)
{
    purchase->setText(equip.at(j+4));
    purchase->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    purchase->setToolTip(TOOL_TIP_ON_BLOCKED_FIELD);
}

void StockTakerEquipList::set_introduced_table_item(int j, QTableWidgetItem *introduced)
{
    introduced->setText(equip.at(j+5));
    introduced->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    introduced->setToolTip(TOOL_TIP_ON_BLOCKED_FIELD);
}

void StockTakerEquipList::set_inspection_table_item(int j, QTableWidgetItem *inspection)
{
    inspection->setText(equip.at(j+6));
    inspection->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    inspection->setToolTip(TOOL_TIP_ON_BLOCKED_FIELD);
}

void StockTakerEquipList::set_branchName_table_item(int j, QTableWidgetItem *branchName)
{
    std::unique_ptr<UserServices> us(new UserServices);
    QString branch=us->findBranch((equip.at(j+7)).toInt());
    branchName->setText(branch);
    branchName->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    branchName->setToolTip(TOOL_TIP_ON_BLOCKED_FIELD);

}

void StockTakerEquipList::get_equipment_data_from_database()
{
    equip = EquipmentService::findAllEquip();
    ui->tableWidget->setRowCount(get_row_quantity());
    ui->tableWidget->setColumnWidth(0, 70);
    ui->tableWidget->setColumnWidth(1, 130);
    ui->tableWidget->setColumnWidth(2, 120);
    ui->tableWidget->setColumnWidth(3, 70);
    ui->tableWidget->setColumnWidth(7, 130);

    int j=0;
    while(j<equip.size()){
        for(int i=0; i<get_row_quantity(); i++){

            ui->tableWidget->setRowHeight(i, 60);

            QTableWidgetItem* id = new QTableWidgetItem();
            QTableWidgetItem* name = new QTableWidgetItem();
            QTableWidgetItem* producent = new QTableWidgetItem();
            QTableWidgetItem* nrInw = new QTableWidgetItem();
            QTableWidgetItem* purchase = new QTableWidgetItem();
            QTableWidgetItem* introduced = new QTableWidgetItem();
            QTableWidgetItem* inspection = new QTableWidgetItem();
            QTableWidgetItem* branchName = new QTableWidgetItem();
            QTableWidgetItem* editNrInw = new QTableWidgetItem();

            set_id_table_item(j,id);
            set_name_table_item(j,name,id);
            set_producent_table_item(j,producent);
            set_nr_inw_table_item(j,nrInw);
            set_purchase_table_item(j,purchase);
            set_introduced_table_item(j,introduced);
            set_inspection_table_item(j,inspection);
            set_branchName_table_item(j,branchName);
            set_edit_button(i, editNrInw);

            ui->tableWidget->setItem(i,0,id);
            ui->tableWidget->setItem(i,1,name);
            ui->tableWidget->setItem(i,2,producent);
            ui->tableWidget->setItem(i,3,nrInw);
            ui->tableWidget->setItem(i,4,purchase);
            ui->tableWidget->setItem(i,5,introduced);
            ui->tableWidget->setItem(i,6,inspection);
            ui->tableWidget->setItem(i,7,branchName);
            ui->tableWidget->setItem(i,8,editNrInw);

            j=j+8;
        }
    }
    connect_edit_equipment_button();
}

StockTakerEquipList::StockTakerEquipList(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StockTakerEquipList)
{
    ui->setupUi(this);

    get_equipment_data_from_database();
}

StockTakerEquipList::~StockTakerEquipList()
{
    delete ui;
}
