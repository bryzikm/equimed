#include "equiplist.h"
#include "ui_equipmentlist.h"
#include "equipment/service/equipmentservice.h"
#include "csingleton.h"

#include <QTableWidget>
#include <QTableWidgetItem>
#include <QString>
#include <QtSql>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include "equipment/renderer/listrenderer.h"

EquipList::EquipList(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EquipList)
{
    ui->setupUi(this);
    QList<QString> equip = EquipmentService::findAllEquip();
    ListRenderer::renderList(this, equip, ui->tableWidget);
}

EquipList::~EquipList()
{
    delete ui;
}

