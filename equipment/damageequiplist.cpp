#include "damageequiplist.h"
#include "ui_damageequiplist.h"
#include "userservices.h"

#include <QMessageBox>
#include <QTableWidget>
#include <QString>
#include <QtSql>
#include <QSqlDatabase>
#include <QSqlQuery>

#include <equipment/service/equipmentservice.h>
#include <equipment/form/repairform.h>
#include <memory>

DamageEquipList::DamageEquipList(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DamageEquipList)
{
    ui->setupUi(this);
    renderDamageEquipList();
}

DamageEquipList::~DamageEquipList()
{
    delete ui;
}

QList<QString> DamageEquipList::get_value_from_row(QTableWidgetItem *item)
{
    QList<QString> equipment_field_value;
    for(int i = 0;i<QUANTITY_OF_VISIBLE_TABLE_VALUE;i++)
        equipment_field_value.append(ui->tableWidget->model()->data(
            ui->tableWidget->model()->index(item->text().toInt(), i)).toString());
    return equipment_field_value;
}

void DamageEquipList::set_id_table_item(int j, QTableWidgetItem *id)
{
    std::unique_ptr<EquipmentService> es(new EquipmentService);
    id->setText(damageEquip.at(j));
    id->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    id->setToolTip(es->generatePath(id));
}

void DamageEquipList::set_name_table_item(int j, QTableWidgetItem *name, QTableWidgetItem *id)
{
    std::unique_ptr<EquipmentService> es(new EquipmentService);
    name->setText(damageEquip.at(j+1));
    name->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    name->setToolTip(es->generatePath(id));
}

void DamageEquipList::set_producent_table_item(int j, QTableWidgetItem *producent)
{
    producent->setText(damageEquip.at(j+2));
    producent->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    producent->setToolTip(TOOL_TIP_ON_BLOCKED_FIELD);
}

void DamageEquipList::set_nr_inw_table_item(int j, QTableWidgetItem *nr_inw)
{
    nr_inw->setText(damageEquip.at(j+3));
    nr_inw->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    nr_inw->setToolTip(TOOL_TIP_ON_BLOCKED_FIELD);
}

void DamageEquipList::set_purchase_table_item(int j, QTableWidgetItem *purchase)
{
    purchase->setText(damageEquip.at(j+4));
    purchase->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    purchase->setToolTip(TOOL_TIP_ON_BLOCKED_FIELD);
}

void DamageEquipList::set_introduced_table_item(int j, QTableWidgetItem *introduced)
{
    introduced->setText(damageEquip.at(j+5));
    introduced->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    introduced->setToolTip(TOOL_TIP_ON_BLOCKED_FIELD);
}

void DamageEquipList::set_inspection_table_item(int j, QTableWidgetItem *inspection)
{
    inspection->setText(damageEquip.at(j+6));
    inspection->setToolTip(TOOL_TIP_ON_EDITABLE_FIELD);
}

void DamageEquipList::set_branchName_table_item(int j, QTableWidgetItem *branchName)
{
    std::unique_ptr<UserServices> us(new UserServices);
    QString branch=us->findBranch((damageEquip.at(j+7)).toInt());
    branchName->setText(branch);
    branchName->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    branchName->setToolTip(TOOL_TIP_ON_BLOCKED_FIELD);

}


void DamageEquipList::renderDamageEquipList() {
    damageEquip = EquipmentService::damageEquipList();

    ui->tableWidget->setIconSize(QSize(this->ICON_WIDTH, this->ICON_HEIGHT));

    ui->tableWidget->setRowCount(damageEquip.size()/8);
    ui->tableWidget->setColumnWidth(0, 70);
    ui->tableWidget->setColumnWidth(1, 130);
    ui->tableWidget->setColumnWidth(2, 120);
    ui->tableWidget->setColumnWidth(3, 70);
    ui->tableWidget->setColumnWidth(7, 130);

    int j = 0;

    while(j < damageEquip.size()){
        for(int i=0; i < this->getRowQuantity(); i++){

            ui->tableWidget->setRowHeight(i, 60);

            QTableWidgetItem* id = new QTableWidgetItem();
            QTableWidgetItem* name = new QTableWidgetItem();
            QTableWidgetItem* producent = new QTableWidgetItem();
            QTableWidgetItem* nrInw = new QTableWidgetItem();
            QTableWidgetItem* purchase = new QTableWidgetItem();
            QTableWidgetItem* introduced = new QTableWidgetItem();
            QTableWidgetItem* inspection = new QTableWidgetItem();
            QTableWidgetItem* branchName = new QTableWidgetItem();
            QTableWidgetItem* editButton = new QTableWidgetItem();
            QTableWidgetItem* repairButton = new QTableWidgetItem();

            QString equipmentId = damageEquip.at(j);

            set_id_table_item(j,id);
            set_name_table_item(j,name, id);
            set_producent_table_item(j,producent);
            set_nr_inw_table_item(j,nrInw);
            set_purchase_table_item(j,purchase);
            set_introduced_table_item(j,introduced);
            set_inspection_table_item(j,inspection);
            set_branchName_table_item(j,branchName);

            setEditButton(i,editButton);
            setRepairButton(repairButton, equipmentId);

            ui->tableWidget->setItem(i,0,id);
            ui->tableWidget->setItem(i,1,name);
            ui->tableWidget->setItem(i,2,producent);
            ui->tableWidget->setItem(i,3,nrInw);
            ui->tableWidget->setItem(i,4,purchase);
            ui->tableWidget->setItem(i,5,introduced);
            ui->tableWidget->setItem(i,6,inspection);
            ui->tableWidget->setItem(i,7,branchName);
            ui->tableWidget->setItem(i,8,editButton);
            ui->tableWidget->setItem(i,9,repairButton);

            j=j+8;
        }

        connectEditButton();
        connectRepairButton();
    }
}

void DamageEquipList::setRepairButton(QTableWidgetItem *item, QString equipmentId) {
    ui->tableWidget->setIconSize(QSize(this->ICON_WIDTH, this->ICON_HEIGHT));
    item->setText(equipmentId);
    item->setSizeHint(QSize(this->ICON_WIDTH, this->ICON_HEIGHT));
    item->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);

    QPixmap pixmap(":/images/button-repair.png");
    QIcon qicon(pixmap);

    item->setIcon(qicon);
}

void DamageEquipList::connectRepairButton() {
    QObject::connect(ui->tableWidget, SIGNAL(itemClicked(QTableWidgetItem*)),
         this, SLOT(repairButtonClicked(QTableWidgetItem*)));
}

QString DamageEquipList::findBranch(int id) {
    std::unique_ptr<UserServices> us(new UserServices);
    QString branchName = us->findBranch(id);
    return branchName;
}

void DamageEquipList::repairButtonClicked(QTableWidgetItem *item) {
    for (int i=0; i < (damageEquip.size()/8); i++)
    {
        if (ui->tableWidget->item(i,DamageEquipList::REPAIR_BUTTON_COLUMN_NUMBER) == item)
        {
            RepairForm repairForm(this, item);
            repairForm.exec();
            DamageEquipList::renderDamageEquipList();
        }
    }
}

void DamageEquipList::setEditButton(int i,QTableWidgetItem *item) {
    ui->tableWidget->setIconSize(QSize(this->ICON_WIDTH, this->ICON_HEIGHT));
    item->setText(QString::number(i));
    item->setSizeHint(QSize(100, 40));
    item->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);

    QPixmap pixmap(":/images/button-edit.png");
    QIcon qicon(pixmap);

    item->setIcon(qicon);
}

void DamageEquipList::editButtonClicked(QTableWidgetItem *item) {
    for (int i=0; i<(damageEquip.size()/8); i++)
    {
        if (ui->tableWidget->item(i,DamageEquipList::EDIT_BUTTON_COLUMN_NUMBER) == item)
        {
            confirm_edit_action(item);
        }
    }
}

void DamageEquipList::connectEditButton() {
    connect(ui->tableWidget, SIGNAL(itemClicked(QTableWidgetItem*)),
         this, SLOT(editButtonClicked(QTableWidgetItem*)));
}

void DamageEquipList::confirm_edit_action(QTableWidgetItem *item)
{
    QList<QString> equipment_field_value = get_value_from_row(item);
    EquipmentService *equip_service = new EquipmentService();
    bool edited_equipment=false;
    QString dateStr = equipment_field_value[6];
    QDate date = QDate::fromString(dateStr,"yyyy-MM-dd");
    if(date.isValid())
        edited_equipment = equip_service->edit_inspection(equipment_field_value[0],dateStr);
    if(edited_equipment)
    {
        QMessageBox::information(this, "Edycja", "Data przeglądu została zaktualizowana");
        DamageEquipList::renderDamageEquipList();
    }
    else
        QMessageBox::critical(this, "Edycja", "  Błąd!  ");

    delete equip_service;
}

int DamageEquipList::getRowQuantity() {
    return damageEquip.size() / valueOfEquip;
}
