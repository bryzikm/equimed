#ifndef LISTRENDERER_H
#define LISTRENDERER_H

#include <QList>
#include <QString>
#include <QTableWidget>
#include "equipment/service/equipmentservice.h"

class ListRenderer : public QObject
{
Q_OBJECT
public:
    static void renderList(QWidget* parent, QList<QString> list, QTableWidget* widget);

private:
    ListRenderer(QWidget* parent, QTableWidget* tableWidget);

    void setupLoanEquipButton(QTableWidgetItem* widgetItem, QString equipmentId);

    void setDamageButton(QTableWidgetItem *item, QString equipmentId);
    void connectDamageButton();
    void connectLoanButton();
    void confirmDamageButtonAction(QTableWidgetItem *item);
    int showDamageButtonMessage(QTableWidgetItem *item);
    static void set_id_table_item(int j, QList<QString> equip, QTableWidgetItem* id);
    static void set_name_table_item(int j, QList<QString> equip, QTableWidgetItem* name, QTableWidgetItem *id);

    QWidget* parent;
    QTableWidget* tableWidget;

    const int DAMAGE_BUTTON_COLUMN_NUMBER = 9;

public slots:
    void loanEquipClicked(QTableWidgetItem *item);

    void damageButtonClicked(QTableWidgetItem *item);
};

#endif // LISTRENDERER_H
