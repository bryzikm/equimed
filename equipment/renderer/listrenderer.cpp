#include "listrenderer.h"
#include "userservices.h"

#include <equipment/service/equipmentservice.h>
#include "equipment/form/loanequipmentform.h"
#include <QMessageBox>
#include <memory>

ListRenderer::ListRenderer(QWidget* parent, QTableWidget* tableWidget) : parent(parent), tableWidget(tableWidget)
{}

void ListRenderer::set_id_table_item(int j,  QList<QString> equip, QTableWidgetItem *id)
{
    std::unique_ptr<EquipmentService> es(new EquipmentService);

    id->setText(equip.at(j));
    id->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    id->setToolTip(es->generatePath(id));
}

void ListRenderer::set_name_table_item(int j,  QList<QString> equip, QTableWidgetItem *name, QTableWidgetItem *id)
{
    std::unique_ptr<EquipmentService> es(new EquipmentService);

    name->setText(equip.at(j+1));
    name->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
    name->setToolTip(es->generatePath(id));
}

void ListRenderer::renderList(QWidget* parent, QList<QString> equip, QTableWidget* tableWidget) {

    ListRenderer* listRenderer = new ListRenderer(parent, tableWidget);

    tableWidget->setIconSize(QSize(100,40));

    int dataOfEquip = 8;
    int size = (equip.size()/dataOfEquip);
    tableWidget->setRowCount(size);
    tableWidget->setColumnWidth(0, 70);
    tableWidget->setColumnWidth(1, 130);
    tableWidget->setColumnWidth(2, 120);
    tableWidget->setColumnWidth(3, 70);
    tableWidget->setColumnWidth(7, 130);

    std::unique_ptr<UserServices> us(new UserServices);

    int j=0;
    while(j<equip.size()){
        for(int i=0; i<size; i++){

            tableWidget->setRowHeight(i, 60);

            QTableWidgetItem* id = new QTableWidgetItem();
            QTableWidgetItem* name = new QTableWidgetItem();
            QTableWidgetItem* producent = new QTableWidgetItem();
            QTableWidgetItem* nrInw = new QTableWidgetItem();
            QTableWidgetItem* purchase = new QTableWidgetItem();
            QTableWidgetItem* introduced = new QTableWidgetItem();
            QTableWidgetItem* inspection = new QTableWidgetItem();
            QTableWidgetItem* branchName = new QTableWidgetItem();
            QTableWidgetItem* loanEquip = new QTableWidgetItem();
            QTableWidgetItem* damageAlert = new QTableWidgetItem();

            set_id_table_item(j,equip,id);
            set_name_table_item(j,equip,name,id);
            QString equipmentId = equip.at(j);
            producent->setText(equip.at(j+2));
            nrInw->setText(equip.at(j+3));
            purchase->setText(equip.at(j+4));
            introduced->setText(equip.at(j+5));
            inspection->setText(equip.at(j+6));
            QString branch=us->findBranch((equip.at(j+7)).toInt());
            branchName->setText(branch);

            listRenderer->setupLoanEquipButton(loanEquip, equipmentId);
            listRenderer->setDamageButton(damageAlert, equipmentId);

            tableWidget->setItem(i,0,id);
            tableWidget->setItem(i,1,name);
            tableWidget->setItem(i,2,producent);
            tableWidget->setItem(i,3,nrInw);
            tableWidget->setItem(i,4,purchase);
            tableWidget->setItem(i,5,introduced);
            tableWidget->setItem(i,6,inspection);
            tableWidget->setItem(i,7,branchName);
            tableWidget->setItem(i,8,loanEquip);
            tableWidget->setItem(i,9,damageAlert);

            j=j+8;
        }
        listRenderer->connectLoanButton();
        listRenderer->connectDamageButton();
    }

}

void ListRenderer::loanEquipClicked(QTableWidgetItem* widgetItem)
{
    int loanColumn=8;
    for (int i = 0; i < this->tableWidget->rowCount(); i++) {
        if (this->tableWidget->item(i, loanColumn) == widgetItem) {

            widgetItem->setSelected(false);
            LoanEquipmentForm loanEquipmentForm(this->parent, widgetItem->text().toInt());
            loanEquipmentForm.exec();
        }
    }
}

void ListRenderer::setupLoanEquipButton(QTableWidgetItem* widgetItem, QString equipmentId)
{
    widgetItem->setText(equipmentId);
    widgetItem->setSizeHint(QSize(100, 40));
    widgetItem->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);

    QPixmap pixmap(":/images/button-loan.png");
    QIcon qicon(pixmap);

    widgetItem->setIcon(qicon);
}

void ListRenderer::setDamageButton(QTableWidgetItem *item, QString equipmentId) {

    item->setText(equipmentId);
    item->setSizeHint(QSize(100, 40));
    item->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);

    QPixmap pixmap(":/images/button-damage.png");
    QIcon qicon(pixmap);

    item->setIcon(qicon);
}

void ListRenderer::confirmDamageButtonAction(QTableWidgetItem *item) {

    if(showDamageButtonMessage(item) == QMessageBox::Ok)
    {
        std::unique_ptr<EquipmentService> equipmentService(new EquipmentService);
        bool returnEquip = equipmentService->reportEquipmentDamage(item->text());

        if(returnEquip){
            QMessageBox::information(parent, "Awaria", "Awaria została zgłoszona");
            ListRenderer::renderList(this->parent, equipmentService->findAllEquip(), this->tableWidget);
        }
        else
            QMessageBox::critical(parent, "Awaria", "Wystąpił błąd!");

    }
}

int ListRenderer::showDamageButtonMessage(QTableWidgetItem *item) {
    QMessageBox messageBox;

    QString message = "Awaria" + item->text() + " zostanie zgłoszona, kontynuować?";

    messageBox.setText(message);
    messageBox.setStandardButtons(QMessageBox::Ok);

    return messageBox.exec();
}

void ListRenderer::connectLoanButton() {

    QObject::connect(tableWidget, SIGNAL(itemClicked(QTableWidgetItem*)),
         this, SLOT(loanEquipClicked(QTableWidgetItem*)));
}

void ListRenderer::connectDamageButton() {
    QObject::connect(tableWidget, SIGNAL(itemClicked(QTableWidgetItem*)),
         this, SLOT(damageButtonClicked(QTableWidgetItem*)));
}

void ListRenderer::damageButtonClicked(QTableWidgetItem *item) {

    for (int i = 0; i < this->tableWidget->rowCount(); i++) {
        if (this->tableWidget->item(i, ListRenderer::DAMAGE_BUTTON_COLUMN_NUMBER) == item) {

           this->confirmDamageButtonAction(item);
        }
    }
}
