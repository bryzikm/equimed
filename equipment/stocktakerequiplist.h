#ifndef STOCKTAKEREQUIPLIST_H
#define STOCKTAKEREQUIPLIST_H

#include <QWidget>
#include "dbmanager.h"
#include "equipment/service/equipmentservice.h"

namespace Ui {
class StockTakerEquipList;
}

class StockTakerEquipList : public QWidget
{
    Q_OBJECT

public:
    explicit StockTakerEquipList(QWidget *parent = 0);

    int get_row_quantity();

    void get_equipment_data_from_database();

    void set_edit_button(int i, QTableWidgetItem* editNrInw);

    void connect_edit_equipment_button();

    void confirm_edit_equipment_action(QTableWidgetItem *item);

    QList<QString> get_value_from_row(QTableWidgetItem* item);

    void set_id_table_item(int j, QTableWidgetItem* id);

    void set_name_table_item(int j, QTableWidgetItem* name, QTableWidgetItem *id);

    void set_producent_table_item(int j, QTableWidgetItem* producent);

    void set_nr_inw_table_item(int j, QTableWidgetItem* nr_inw);

    void set_purchase_table_item(int j, QTableWidgetItem* purchase);

    void set_introduced_table_item(int j, QTableWidgetItem* introduced);

    void set_inspection_table_item(int j, QTableWidgetItem* inspection);

    void set_branchName_table_item(int j, QTableWidgetItem* branchName);

    ~StockTakerEquipList();

public slots:
    void edit_button_clicked(QTableWidgetItem *item);

private:
    Ui::StockTakerEquipList *ui;
    DbManager *dbManager;
    QList<QString> equip;
    int dataOfEquip = 8;
    const int BUTTON_WIDTH = 100;
    const int BUTTON_HEIGHT = 40;
    const int QUANTITY_OF_VISIBLE_TABLE_VALUE = 7;
    const int EDIT_BUTTON_COLUMN_NUMBER = 8;
    const QString TOOL_TIP_ON_BLOCKED_FIELD = "To pole nie może zostać zmienione.";
    const QString TOOL_TIP_ON_EDITABLE_FIELD = "To pole może zostać zmienione.";
};

#endif // STOCKTAKEREQUIPLIST_H
