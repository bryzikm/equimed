#ifndef DBMANAGER_H
#define DBMANAGER_H
#include <QtSql>
#include <QtDebug>
#include <QFileInfo>

class DbManager
{
public:
    DbManager();
    QSqlDatabase mydb;
    bool open_connection();
    bool close_connection();
};

#endif // DBMANAGER_H
