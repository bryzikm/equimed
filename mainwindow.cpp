#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "userlogin.h"
#include "usermenu.h"
#include <QWidget>
#include "userdatawidget.h"
#include <QPushButton>
#include <QHBoxLayout>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //Background

    QPixmap bg(":/images/medical_bg1.jpg");
    bg = bg.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Background, bg);
    this->setPalette(palette);
    UserMenu *usermenu = new UserMenu();
    usermenu->createMenu(this, CSingleton::getInstance()->CheckRole());

    UserDataWidget *widget= new UserDataWidget;
    this->addWidgets(widget);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::remove_existing_widgets()
{
    while ( QWidget* w = ui->centralWidget->findChild<QWidget*>() )
        delete w;
}

void MainWindow::addWidgets(QWidget *widget)
{
    remove_existing_widgets();
    this->ui->gridLayout->setMargin(100);
    this->ui->gridLayout->addWidget(widget,0,0,1,2);
}
