#ifndef USERMENU_H
#define USERMENU_H

#include <QObject>
#include <QWidget>
#include <QMenu>
#include <QMenuBar>
#include <QAction>
#include <QList>
#include <QMainWindow>
#include "mainwindow.h"
#include "userdatawidget.h"

class UserMenu : public QMainWindow
{

    Q_OBJECT
public:
    UserMenu();
    MainWindow *mw;
    UserDataWidget *widget;
    void createMenu(MainWindow *mainwindow, QString role);

public slots:
    void actionEvent(QAction* act);

private:
    void mainPage();
    void addEquipment();
    void logout();
    void userList();
    void equipmentList();
    void addUser();
    void loanEquipList();
    void searchEquipment();
    void damageEquipList();

};


#endif // USERMENU_H
