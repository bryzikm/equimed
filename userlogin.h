#ifndef USERLOGIN_H
#define USERLOGIN_H

#include <QMainWindow>
#include <QtSql>
#include <QtDebug>
#include <QFileInfo>
#include "mainwindow.h"
#include "csingleton.h"
#include "dbservices.h"
#include "userservices.h"

namespace Ui {
class userLogin;
}

class userLogin : public QMainWindow
{
    Q_OBJECT

private:
    QString login,password;

public:
    explicit userLogin(QWidget *parent = 0);
    ~userLogin();

private slots:
    void on_pushButtonLogin_clicked();

private:
    Ui::userLogin *ui;
};

#endif // USERLOGIN_H
